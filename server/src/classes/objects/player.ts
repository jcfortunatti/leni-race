import { Collideable, CircleCollideable } from "../physics";
import { playerRadius, ROCKET_COOLDOWN, MAX_ROCKETS } from "../../globals";
import { Vector } from "../math";
import { Body } from "matter-js";
import { SPlayer } from "../../types";
import { PlayerCategory } from "./categories";


export default class Player extends Collideable {
    _id: string
    _name: string
    _pointingVector: Vector
    _direction: Vector
    _acceleration: number
    _exploded: boolean
    _life: number
    _initialPosition: Vector
    _boosted: boolean

    _rockets: number
    _rocketCooldown: number

    constructor(id: string, name: string, position: Vector) {
        super(position);

        this._body = new CircleCollideable(10, position, playerRadius, {
            mass: 10,
            restitution: 0.2,
            frictionStatic: 0,
            friction: 0.5,
            frictionAir: 0.07,
            collisionFilter: {
                category: PlayerCategory
            }
        })._body;

        // this._body.plugin.owner = this;

        this._pointingVector = new Vector(0, -1);
        this._direction = new Vector(0, 0);
        this._initialPosition = position

        this._id = id;
        this._name = name;
        this._acceleration = 0.07
        this._body.plugin.owner = this;
        this._exploded = false;
        this._life = 100;
        this._boosted = false;
        this._rockets = 5;
        this._rocketCooldown = ROCKET_COOLDOWN;
    }

    changeDirection(direction: Vector): void {
        this._direction = direction
        if (direction.x !== 0 || direction.y !== 0) {
            this._pointingVector = direction
        }
    }

    update(dt: number): void {
        if (this._direction.module()) {
            const forcePosition = this._body.position
            const force = this._direction.normalize().multiply(this._acceleration)
            Body.applyForce(this._body, forcePosition, force);
        }
        this._updateRockets(dt)
        super.update(dt);
    }

    _updateRockets(dt: number): void {
        if (this._rockets < MAX_ROCKETS) {
            this._rocketCooldown -= dt / 1000;
            if (this._rocketCooldown <= 0) {
                this._rockets += 1
                this._rocketCooldown = ROCKET_COOLDOWN;
            }
        }
    }

    shoot(): boolean {
        if (this._rockets > 0) {
            this._rockets--
            return true
        }
        return false;
    }

    hit(): void {
        this._life -= 25;
        if (this._life <= 0) this._exploded = true;
        if (this._exploded) {
            setTimeout(() => {
                this.setPosition(this._initialPosition);
                this._exploded = false;
                this._life = 100;
            }, 2000);
        }
    }

    _boostTimeout?: NodeJS.Timeout;
    boost(): void {
        this._acceleration = 0.14
        this._boosted = true

        if (this._boostTimeout) clearTimeout(this._boostTimeout)
        this._boostTimeout = setTimeout(() => {
            this._acceleration = 0.07;
            this._boosted = false;
        }, 3000);
    }

    serialize(): SPlayer {
        return {
            type: 'player',
            id: this._id,
            name: this._name,
            exploded: this._exploded,
            angle: 0,
            position: this.getPosition().serialize(),
            direction: this._direction.serialize(),
            life: this._life,
            boosted: this._boosted,
            rockets: this._rockets,
            rocketCooldown: Math.round(this._rocketCooldown * 100 / ROCKET_COOLDOWN)
        }
    }
}
import { Collideable, CircleCollideable } from "../physics";
import { playerRadius } from "../../globals";
import { Vector } from "../math";
import { Body } from "matter-js";
import { SRocket, ICollideableBody } from "../../types";
import Player from "./player";
import { RocketCategory } from "./categories";


export default class Rocket extends Collideable {
    _owner: Player

    constructor(owner: Player) {
        super(owner.getPosition());

        this._body = new CircleCollideable(9999, owner.getPosition(), playerRadius, {
            mass: 9999,
            inertia: 9999,
            friction: 0,
            frictionAir: 0,
            frictionStatic: 0,
            isSensor: true,
            collisionFilter: {
                category: RocketCategory
            },
            plugin: {
                owner: owner,
                duration: 2500,
                onCollision: this._handleCollision
            }
        })._body;

        this.setVelocity(owner.getVelocity().add(owner._pointingVector.normalize().multiply(20)));

        this._owner = owner;
    }

    _handleCollision = (body: ICollideableBody): void => {
        this.dematerialize()
    }

    serialize(): SRocket {
        return {
            type: 'rocket',
            angle: this.getAngle(),
            position: this.getPosition().serialize(),
            direction: this.getVelocity().normalize()
        }
    }
}
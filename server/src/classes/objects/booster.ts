import { Collideable, CircleCollideable } from "../physics";
import { boosterRadius } from "../../globals";
import { Vector } from "../math";
import { SBooster, ICollideableBody } from "../../types";
import { BoosterCategory } from "./categories";


export default class Booster extends Collideable {

    constructor(position: Vector) {
        super(position);

        this._body = new CircleCollideable(9999, position, boosterRadius, {
            mass: 9999,
            inertia: 9999,
            friction: 0,
            frictionAir: 0,
            frictionStatic: 0,
            isSensor: true,
            collisionFilter: {
                category: BoosterCategory
            },
            plugin: {
                duration: 10000,
                onCollision: this._handleCollision
            }
        })._body;
    }

    _handleCollision = (body: ICollideableBody): void => {
        this.dematerialize()
    }

    serialize(): SBooster {
        return {
            type: 'booster',
            position: this.getPosition().serialize(),
        }
    }
}
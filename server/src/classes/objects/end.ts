import { RectCollideable } from "../physics";
import { Vector } from "../math";
import { IChamferableBodyDefinition } from "matter-js";
import { EndCategory } from "./categories";

export default class End extends RectCollideable {
    constructor(position: Vector, width: number, height: number, options?: IChamferableBodyDefinition) {
        super(0, new Vector(position.x + width / 2, position.y + height / 2),
            width, height, 0,
            {
                isStatic: true,
                isSensor: true,
                ...(options ? options : {}),
                collisionFilter: {
                    category: EndCategory,
                    ...(options && options.collisionFilter ? options.collisionFilter : {})
                }
            }
        );
    }
}
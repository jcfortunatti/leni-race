import { listen } from "socket.io"


export class Eventable {
    _listeners: { [key: string]: ((...args: any[]) => void)[] } = {}

    constructor() {
    }

    listen(eventName: string, callback: (...args: any[]) => void) {
        if (!this._listeners[eventName]) this._listeners[eventName] = []
        this._listeners[eventName].push(callback)
    }

    _trigger(eventName: string, ...args: any[]) {
        if (this._listeners[eventName]) {
            this._listeners[eventName].forEach(listener => {
                listener(...args);
            })
        }
    }
}
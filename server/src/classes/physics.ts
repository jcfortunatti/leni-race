import { Bodies, Body, World } from 'matter-js'
import { Vector } from './math'
import { ICollideableBody, SCollideable, SPolygonCollideable, SRectCollideable, SCircleCollideable } from '../types'
import { elapsedSeconds } from '../utils';
import { Eventable } from './events';
import { v4 as uuid } from 'uuid'

export class Collideable extends Eventable {
    _id: string
    _body: ICollideableBody;

    _world?: World;
    _mounted: boolean = false
    _mountedDT?: Date

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(_position: Vector) {
        super();
        this._id = uuid();
        this._body = Bodies.circle(0, 0, 0, {});
    }

    isExpired = (): boolean => {
        const { duration } = this._body.plugin
        if (duration && this._mounted && this._mountedDT && elapsedSeconds(this._mountedDT) * 1000 > duration)
            return true
        return false;
    }

    materialize(world: World): void {
        if (!this._mounted) {
            this._world = world
            this._mounted = true;
            this._mountedDT = new Date();
            World.add(world, this._body);
            this._trigger('materialize')
        }
    }

    dematerialize(): void {
        if (this._mounted && this._world) {
            this._mounted = false;
            World.remove(this._world, this._body);
            this._trigger('dematerialize')
        }
    }

    setVelocity(velocity: Vector, angularVelocity: number = 0): void {
        if (this._body) {
            Body.setVelocity(this._body, velocity);
            Body.setAngularVelocity(this._body, angularVelocity);
        }
    }

    getVelocity(): Vector {
        return Vector.fromMatter(this._body.velocity)
    }

    setPosition(position: Vector): void {
        Body.setPosition(this._body, position);
    }

    getAngle(): number {
        return this._body.angle;
    }

    setAngle(angle: number): void {
        Body.setAngle(this._body, angle)
    }

    getPosition(): Vector {
        return Vector.fromMatter(this._body.position);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    update(_dt: number): void {
        if (this._body.speed > 0 && this._body.speed < 0.01) {
            this.setVelocity(new Vector(0, 0));
        }
    }

    getPoints(): Vector[] {
        if (this._body.parts.length > 1)
            return this._body.parts.map(b => Vector.fromMatter(b.position))
        return this._body.vertices.map(v => Vector.fromMatter(v))
    }

    applyForce(force: Vector, position: Vector): void {
        Body.applyForce(this._body, position, force)
    }

    serialize(): SCollideable<string> {
        return {
            type: 'none',
            position: this.getPosition().serialize(),
            angle: this.getAngle(),
            direction: this.getVelocity().director()
        }
    }
}

export class CircleCollideable extends Collideable {
    constructor(mass: number, position: Vector, radius: number,
        options?: Matter.IBodyDefinition) {
        super(position);
        this._body = Bodies.circle(position.x, position.y, radius, {
            plugin: {
                owner: this,
            },
            mass,
            ...(options ? options : {}),
        })
    }

    serialize(): SCircleCollideable {
        return {
            type: 'circle',
            position: this.getPosition().serialize(),
            angle: this.getAngle(),
            radius: this._body.circleRadius || 0,
            direction: this.getVelocity().director()
        }
    }
}

export class RectCollideable extends Collideable {
    _width: number
    _height: number

    constructor(mass: number,
        position: Vector,
        width: number,
        height: number,
        angle: number = 0,
        options?: Matter.IChamferableBodyDefinition) {
        super(position);

        this._width = width
        this._height = height

        this._body = Bodies.rectangle(position.x, position.y, width, height, {
            plugin: {
                owner: this,
            },
            mass,
            angle,
            ...(options ? options : {}),
        })
    }

    serialize(): SRectCollideable {
        return {
            type: 'rect',
            position: this.getPosition().serialize(),
            angle: this.getAngle(),
            width: this._width,
            height: this._height,
            direction: this.getVelocity().director()
        }
    }
}

export class PolygonCollideable extends Collideable {
    constructor(mass: number,
        position: Vector,
        points: Vector[][],
        options?: Matter.IBodyDefinition) {
        super(position);

        this._body = Bodies.fromVertices(position.x, position.y, points, {
            plugin: {
                owner: this,
            },
            mass,
            ...(options ? options : {}),
        })
    }

    serialize(): SPolygonCollideable {
        return {
            type: 'polygon',
            position: this.getPosition().serialize(),
            angle: this.getAngle(),
            points: this.getPoints(),
            direction: this.getVelocity().director()
        }
    }
}
import { Vector as MatterVector } from 'matter-js'
import { SVector } from '../types';

export class Vector {
    x: number
    y: number
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    static fromMatter(v: MatterVector): Vector {
        return new Vector(v.x, v.y);
    }

    setX(x: number): Vector {
        return new Vector(x, this.y);
    }

    setY(y: number): Vector {
        return new Vector(this.x, y);
    }

    module(): number {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    normalize(): Vector {
        const module = this.module();
        if (module > 0) return new Vector(this.x / module, this.y / module);
        return this;
    }

    add(v: Vector): Vector {
        return new Vector(this.x + v.x, this.y + v.y);
    }

    substract(v: Vector): Vector {
        return new Vector(this.x - v.x, this.y - v.y);
    }

    multiply(n: number): Vector {
        return new Vector(this.x * n, this.y * n);
    }

    invert(): Vector {
        return this.multiply(-1);
    }

    director(): Vector {
        return new Vector(this.x !== 0 ? this.x / Math.abs(this.x) : 0, this.y !== 0 ? this.y / Math.abs(this.y) : 0);
    }

    dot(v: Vector): number {
        return this.x * v.x + this.y * v.y;
    }

    angle(v?: Vector): number {
        if (v) return Math.acos(this.dot(v) / (this.module() * v.module()));
        if (this.x === 0 && this.y > 0) return Math.PI / 2;
        if (this.x === 0 && this.y < 0) return -Math.PI / 2;
        return Math.atan(this.y / this.x) - (this.x < 0 ? Math.PI : 0);
    }

    rotate(angle: number): Vector {
        return new Vector(
            this.x * Math.cos(angle) - this.y * Math.sin(angle),
            this.x * Math.sin(angle) - this.y * Math.cos(angle)
        );
    }

    copy(): Vector {
        return new Vector(this.x, this.y);
    }

    isEqual(v: Vector): boolean {
        return (this.x === v.x && this.y === v.y);
    }

    toArray(): [number, number] {
        return [this.x, this.y];
    }

    serialize(): SVector {
        return {
            x: this.x,
            y: this.y
        };
    }
}

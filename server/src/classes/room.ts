import Player from "./objects/player"
import { Vector } from "./math"
import { timeConstant, mapSize } from "../globals"
import { Engine, World, Events } from 'matter-js'
import { ICollideableEventCollision, SRoom, SPlayer, ICollideableBody } from "../types"
import Wall from "./objects/wall"
import End from "./objects/end"
import Booster from "./objects/booster"
import { RectCollideable, Collideable } from "./physics"
import { EndCategory, PlayerCategory, RocketCategory, BoosterCategory } from "./objects/categories"
import { elapsedSeconds, round } from "../utils"
import Rocket from "./objects/rocket"

export class Room {
    _id: string
    _name: string
    _socket: SocketIO.Namespace
    _players: { [x: string]: Player }
    _effects: Collideable[] = []
    _interval?: NodeJS.Timeout
    _engine?: Engine
    _world?: World

    _startDate?: Date;
    _endDate?: Date;
    _countdown: number = 0;
    _resetCountdown: number = 10;
    _playing: boolean = false;

    _walls: RectCollideable[] = [
        new Wall(new Vector(0, -100), mapSize.x, 100),
        new Wall(new Vector(0, mapSize.y), mapSize.x, 100),
        new Wall(new Vector(-100, 0), 100, mapSize.y),
        new Wall(new Vector(mapSize.x, 0), 100, mapSize.y),
    ]

    _startWall: RectCollideable = new Wall(new Vector(0, mapSize.y - 500), mapSize.x, 100);

    _endSensor: RectCollideable = new End(new Vector(0, 500), mapSize.x, 100);

    constructor(id: string, name: string, socket: SocketIO.Namespace) {
        this._id = id;
        this._name = name;
        this._players = {};
        this._socket = socket;
        this._initializeEngine();
        this._handleSocket(socket);
    }

    //socket BEGIN
    _emit = (): void => {
        this._socket.emit('update', this.serialize());
    }

    _handleSocket = (socket: SocketIO.Namespace): void => {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        socket.on('connection', function (clientSocket) {
            const playerId = clientSocket.id;
            const { name } = clientSocket.handshake.query;

            const player = self._addPlayer(playerId, name);
            clientSocket.emit('login_success', { self: player.serialize(), ...self.serialize(), });
            clientSocket.broadcast.emit('player_join', player.serialize());

            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            clientSocket.on('disconnect', function (_) {
                self._removePlayer(playerId);
                clientSocket.broadcast.emit('player_leave', { id: playerId })
            });

            clientSocket.on('request_direction_change', function (payload) {
                self._playerDirectionChanged(playerId, payload.direction);
            });

            clientSocket.on('request_key_press', function (payload) {
                self._playerKeyPress(playerId, payload.keyCode);
            });

            clientSocket.on('request_send_message', function (payload) {
                socket.emit('player_message', { name: player._name, message: payload.message });
            });

            if (self._world)
                player.materialize(self._world);
        });
    }
    //socket END

    //players BEGIN
    _connectedPlayers(): Player[] {
        return Object.keys(this._players).reduce((r: Player[], key: string) => {
            if (this._socket.clients(() => true).connected[key]) {
                r.push(this._players[key]);
            }
            else {
                delete this._players[key];
            }
            return r;
        }, [])
    }

    _addPlayer(id: string, name: string): Player {
        if (Object.entries(this._players).length === 0) this._begin();
        this._players[id] = new Player(id, name, mapSize.setX(Math.random() * mapSize.x).setY(mapSize.y - Math.random() * 100 - 50));
        return this._players[id];
    }

    _removePlayer(id: string): void {
        if (this._players[id]) {
            this._players[id].dematerialize();
        }

        this._players = Object.keys(this._players).reduce((result: { [key: string]: Player }, key: string) => {
            if (key !== id) {
                result[key] = this._players[key];
            }
            else {
                delete this._players[key];
            }
            return result;
        }, {});

        if (this._connectedPlayers().length === 0) this._restore()
    }

    _playerDirectionChanged(id: string, { x, y }: { x: number, y: number }): void {
        if (this._players[id]) {
            this._players[id].changeDirection(new Vector(x, y));
        }
    }

    async _playerKeyPress(id: string, code: string): Promise<void> {
        if (this._playing) {
            if (code === 'Space') {
                const player = this._players[id];
                if (player.shoot()) {
                    const rocket = new Rocket(player);
                    rocket.listen('dematerialize', () => {
                        this._effects = this._effects.filter(effect => effect._id !== rocket._id)
                    })
                    this._effects.push(rocket);
                    if (this._world) rocket.materialize(this._world);
                }
            }
        }
    }
    //players END

    //engine BEGIN
    _initializeEngine = (): void => {
        this._engine = Engine.create();
        this._world = this._engine.world;
        this._world.gravity.x = 0;
        this._world.gravity.y = 0;

        Events.on(this._engine, 'collisionStart', this._handleCollisionsStart);
        Events.on(this._engine, 'collisionEnd', this._handleCollisionsEnd);

        let i = 0;
        this._interval = setInterval(() => {
            i++;
            if (this._update() || (1000 / timeConstant) <= i) {
                i = 0;
                this._emit();
            }
        }, timeConstant);

        this._mountWalls();
        this._mountSensors();
    }

    _handleCollisionsStart = (e: ICollideableEventCollision): void => {
        const pairs = e.pairs;
        for (let i = 0, j = pairs.length; i != j; ++i) {
            const pair = pairs[i];
            this._handleGameCollision(pair.bodyA, pair.bodyB);
            this._handleGameCollision(pair.bodyB, pair.bodyA);
        }
    }

    _handleCollisionsEnd = (e: ICollideableEventCollision): void => {
        const pairs = e.pairs;
        for (let i = 0, j = pairs.length; i != j; ++i) {
            // const pair = pairs[i];
        }
    }

    _update(): boolean {
        if (this._engine && this._world) {
            Engine.update(this._engine);
            this._connectedPlayers().forEach(player => player.update(timeConstant));
            if (this._playing && Math.random() > 0.9) this._generateBooster()
            this._effects = this._effects.reduce((r: Collideable[], effect) => {
                if (effect.isExpired()) effect.dematerialize()
                else {
                    effect.update(timeConstant)
                    r.push(effect)
                }
                return r
            }, [])
            return true //this._world.bodies.some(x => x.speed > 0);
        }
        return false;
    }
    //physics END

    //game BEGIN
    _placements: Player[] = []
    _mountWalls = (): void => {
        this._walls.forEach(wall => {
            if (this._world) wall.materialize(this._world)
        })
        if (this._world) this._startWall.materialize(this._world);
    }

    _generateBooster = (): void => {
        const booster = new Booster(new Vector(mapSize.x * Math.random(), mapSize.y * Math.random()))
        booster.listen('dematerialize', () => {
            this._effects = this._effects.filter(effect => effect._id !== booster._id)
        })
        this._effects.push(booster);
        if (this._world) booster.materialize(this._world);
    }

    _mountStartWalls = (): void => {
        console.log('_mountStartWalls')
        if (this._world) this._startWall.materialize(this._world);
    }

    _unmountStartWalls = (): void => {
        console.log('_unmountStartWalls')
        if (this._world) this._startWall.dematerialize();
    }

    _mountSensors = (): void => {
        if (this._world) this._endSensor.materialize(this._world)
    }

    _handleGameCollision = (bodyA: ICollideableBody, bodyB: ICollideableBody): void => {
        if (bodyA.isSensor && bodyA.collisionFilter.category === EndCategory && bodyB.collisionFilter.category === PlayerCategory) {
            if (bodyB.plugin.owner) {
                this._playerEndRace(bodyB.plugin.owner);
            }
        }

        if (bodyA.isSensor && bodyA.collisionFilter.category === RocketCategory && bodyB.collisionFilter.category === PlayerCategory) {
            if (bodyA.plugin.owner && bodyB.plugin.owner && bodyA.plugin.owner._id !== bodyB.plugin.owner._id) {
                console.log("hit")
                bodyB.plugin.owner.hit()
                if (bodyA.plugin.onCollision) bodyA.plugin.onCollision(bodyB)
            }
        }

        if (bodyA.isSensor && bodyA.collisionFilter.category === BoosterCategory && bodyB.collisionFilter.category === PlayerCategory) {
            console.log("boosted")
            bodyB.plugin.owner?.boost()
            if (bodyA.plugin.onCollision) bodyA.plugin.onCollision(bodyB)
        }
    }

    _playerEndRace = (player: Player): void => {
        if (!this._placements.some(x => x._id === player._id)) {
            this._placements.push(player);
            player._acceleration = 0;
        }
        if (this._placements.length === this._connectedPlayers().length) {
            this._reset();
        }
    }

    _begin = (): void => {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        this._startDate = new Date();
        setTimeout(() => {
            console.log("_countdown")
            this._playing = true;
            self._startDate = undefined;
            self._endDate = undefined;
            self._unmountStartWalls();
        }, this._countdown * 1000);
    }

    _reset = (): void => {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        this._endDate = new Date();
        setTimeout(() => {
            self._restore()
            if (this._connectedPlayers().length > 0) {
                self._begin();
            }
        }, this._resetCountdown * 1000);
    }

    _restore = (): void => {
        this._playing = false;
        this._startDate = undefined;
        this._endDate = undefined;
        this._placements = [];
        this._effects.forEach(effect => {
            effect.dematerialize();
        })
        this._effects = []
        this._connectedPlayers().map(player => {
            player.setPosition(new Vector(Math.random() * mapSize.x, mapSize.y - Math.random() * 100 - 50))
            player._acceleration = 0.07;
        });
        this._mountStartWalls()
    }

    //game END

    _getCountdown(): undefined | number {
        if (this._endDate) {
            return Math.round(this._resetCountdown - elapsedSeconds(this._endDate, new Date()))
        }
        else if (this._startDate) {
            return Math.round(this._countdown - elapsedSeconds(this._startDate, new Date()))
        }
        return 0
    }

    serialize(): SRoom {
        return {
            players: Object.entries(this._players)
                .reduce((result: { [key: string]: SPlayer }, [key, player]) => {
                    result[key] = player.serialize()
                    return result
                }, {}),
            effects: this._effects.map(effect => effect.serialize()),
            placements: this._placements.map(player => player.serialize()),
            countdown: this._getCountdown(),
            gameEnded: this._endDate ? true : false,
        };
    }
}
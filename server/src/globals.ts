import { Vector } from "./classes/math";

export const DEBUG: boolean = false;

export const timeConstant: number = 25;
export const playerRadius: number = 40;
export const boosterRadius: number = 30;
export const mapSize: Vector = new Vector(2000, 50000);


export const ROCKET_COOLDOWN = 1.5;
export const MAX_ROCKETS = 5;
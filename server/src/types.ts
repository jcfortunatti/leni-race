import { IEventCollision, Engine, IPair, Body } from "matter-js"
import Player from "./classes/objects/player"

export interface ICollideableBody extends Body {
    plugin: {
        owner?: Player
        duration?: number
        onCollision?: (body: ICollideableBody) => void
    }
}

export interface ICollideablePair extends IPair {
    bodyA: ICollideableBody;
    bodyB: ICollideableBody;
}

export interface ICollideableEventCollision extends IEventCollision<Engine> {
    pairs: Array<ICollideablePair>;
}

export interface IChatMessage {
    player: Player
    message: string
    date: Date
}

//serialized types / interfaces
export type SVector = {
    x: number,
    y: number
}

export interface SCollideable<T extends string> {
    type: 'none' | T
    position: SVector
    angle?: number
    direction?: SVector
}

export interface SCircleCollideable extends SCollideable<'circle'> {
    type: 'circle'
    radius: number
}

export interface SRectCollideable extends SCollideable<'rect'> {
    type: 'rect'
    width: number
    height: number
}

export interface SPolygonCollideable extends SCollideable<'polygon'> {
    type: 'polygon'
    points: SVector[]
}

export interface SPlayer extends SCollideable<'player'> {
    type: 'player'
    id: string
    name: string
    exploded: boolean
    life: number
    boosted: boolean
    rockets: number
    rocketCooldown: number
}

export interface SRocket extends SCollideable<'rocket'> {
    type: 'rocket'
}

export interface SBooster extends SCollideable<'booster'> {
    type: 'booster'
}

export interface SRoom {
    players: { [key: string]: SPlayer }
    effects: SCollideable<any>[]
    placements: SPlayer[]
    gameEnded: boolean
    countdown?: number
}
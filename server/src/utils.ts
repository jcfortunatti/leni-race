const roundingDecimals: number = 3;

export const reverseArray = <T>(arr: Array<T>): Array<T> => {
    return arr.reduce((result: Array<T>, e: T, i: number) => {
        result[arr.length - i - 1] = e;
        return result;
    }, []);
}

export const elapsedSeconds = (from: Date, to?: Date): number => {
    if (!to) {
        return (new Date().getTime() - from.getTime()) / 1000;
    }
    return (to.getTime() - from.getTime()) / 1000;
}

export const round = (n: number): number => Math.round(n * roundingDecimals) / roundingDecimals;

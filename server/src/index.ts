import * as express from 'express';
import * as socketio from 'socket.io'
import { Room } from './classes/room';
import { Server as HttpServer } from 'http';

const ENV_DEVELOPMENT = process.env.NODE_ENV === "development";

console.log(ENV_DEVELOPMENT ? "DEVELOPMENT ENVIRONMENT" : "PRODUCTION ENVIRONMENT");

const app = express();

const http = new HttpServer(app);
const io = socketio(http, { path: '/ws' });

const roomId = '/always-open';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const matches = {
  [roomId]: new Room(roomId, "Always open", io.of(roomId))
};

http.listen(3000, function () {
  console.log('started on port 3000');
  process.on("SIGINT", closeApp);
  process.on("SIGTERM", closeApp);
});

function closeApp() {
  process.exit(0);
}

export default app
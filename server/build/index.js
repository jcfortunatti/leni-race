/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/uuid/dist/esm-node/index.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/index.js ***!
  \**************************************************/
/*! exports provided: v1, v3, v4, v5, NIL, version, validate, stringify, parse */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _v1_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v1.js */ \"./node_modules/uuid/dist/esm-node/v1.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"v1\", function() { return _v1_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n/* harmony import */ var _v3_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./v3.js */ \"./node_modules/uuid/dist/esm-node/v3.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"v3\", function() { return _v3_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]; });\n\n/* harmony import */ var _v4_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./v4.js */ \"./node_modules/uuid/dist/esm-node/v4.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"v4\", function() { return _v4_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"]; });\n\n/* harmony import */ var _v5_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./v5.js */ \"./node_modules/uuid/dist/esm-node/v5.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"v5\", function() { return _v5_js__WEBPACK_IMPORTED_MODULE_3__[\"default\"]; });\n\n/* harmony import */ var _nil_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./nil.js */ \"./node_modules/uuid/dist/esm-node/nil.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"NIL\", function() { return _nil_js__WEBPACK_IMPORTED_MODULE_4__[\"default\"]; });\n\n/* harmony import */ var _version_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./version.js */ \"./node_modules/uuid/dist/esm-node/version.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"version\", function() { return _version_js__WEBPACK_IMPORTED_MODULE_5__[\"default\"]; });\n\n/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./validate.js */ \"./node_modules/uuid/dist/esm-node/validate.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"validate\", function() { return _validate_js__WEBPACK_IMPORTED_MODULE_6__[\"default\"]; });\n\n/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./stringify.js */ \"./node_modules/uuid/dist/esm-node/stringify.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"stringify\", function() { return _stringify_js__WEBPACK_IMPORTED_MODULE_7__[\"default\"]; });\n\n/* harmony import */ var _parse_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./parse.js */ \"./node_modules/uuid/dist/esm-node/parse.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"parse\", function() { return _parse_js__WEBPACK_IMPORTED_MODULE_8__[\"default\"]; });\n\n\n\n\n\n\n\n\n\n\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/index.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/md5.js":
/*!************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/md5.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto */ \"crypto\");\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_0__);\n\n\nfunction md5(bytes) {\n  if (Array.isArray(bytes)) {\n    bytes = Buffer.from(bytes);\n  } else if (typeof bytes === 'string') {\n    bytes = Buffer.from(bytes, 'utf8');\n  }\n\n  return crypto__WEBPACK_IMPORTED_MODULE_0___default.a.createHash('md5').update(bytes).digest();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (md5);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/md5.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/nil.js":
/*!************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/nil.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ('00000000-0000-0000-0000-000000000000');\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/nil.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/parse.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/parse.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validate.js */ \"./node_modules/uuid/dist/esm-node/validate.js\");\n\n\nfunction parse(uuid) {\n  if (!Object(_validate_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(uuid)) {\n    throw TypeError('Invalid UUID');\n  }\n\n  let v;\n  const arr = new Uint8Array(16); // Parse ########-....-....-....-............\n\n  arr[0] = (v = parseInt(uuid.slice(0, 8), 16)) >>> 24;\n  arr[1] = v >>> 16 & 0xff;\n  arr[2] = v >>> 8 & 0xff;\n  arr[3] = v & 0xff; // Parse ........-####-....-....-............\n\n  arr[4] = (v = parseInt(uuid.slice(9, 13), 16)) >>> 8;\n  arr[5] = v & 0xff; // Parse ........-....-####-....-............\n\n  arr[6] = (v = parseInt(uuid.slice(14, 18), 16)) >>> 8;\n  arr[7] = v & 0xff; // Parse ........-....-....-####-............\n\n  arr[8] = (v = parseInt(uuid.slice(19, 23), 16)) >>> 8;\n  arr[9] = v & 0xff; // Parse ........-....-....-....-############\n  // (Use \"/\" to avoid 32-bit truncation when bit-shifting high-order bytes)\n\n  arr[10] = (v = parseInt(uuid.slice(24, 36), 16)) / 0x10000000000 & 0xff;\n  arr[11] = v / 0x100000000 & 0xff;\n  arr[12] = v >>> 24 & 0xff;\n  arr[13] = v >>> 16 & 0xff;\n  arr[14] = v >>> 8 & 0xff;\n  arr[15] = v & 0xff;\n  return arr;\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (parse);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/parse.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/regex.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/regex.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/regex.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/rng.js":
/*!************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/rng.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return rng; });\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto */ \"crypto\");\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_0__);\n\nconst rnds8Pool = new Uint8Array(256); // # of random values to pre-allocate\n\nlet poolPtr = rnds8Pool.length;\nfunction rng() {\n  if (poolPtr > rnds8Pool.length - 16) {\n    crypto__WEBPACK_IMPORTED_MODULE_0___default.a.randomFillSync(rnds8Pool);\n    poolPtr = 0;\n  }\n\n  return rnds8Pool.slice(poolPtr, poolPtr += 16);\n}\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/rng.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/sha1.js":
/*!*************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/sha1.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto */ \"crypto\");\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_0__);\n\n\nfunction sha1(bytes) {\n  if (Array.isArray(bytes)) {\n    bytes = Buffer.from(bytes);\n  } else if (typeof bytes === 'string') {\n    bytes = Buffer.from(bytes, 'utf8');\n  }\n\n  return crypto__WEBPACK_IMPORTED_MODULE_0___default.a.createHash('sha1').update(bytes).digest();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (sha1);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/sha1.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/stringify.js":
/*!******************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/stringify.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validate.js */ \"./node_modules/uuid/dist/esm-node/validate.js\");\n\n/**\n * Convert array of 16 byte values to UUID string format of the form:\n * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX\n */\n\nconst byteToHex = [];\n\nfor (let i = 0; i < 256; ++i) {\n  byteToHex.push((i + 0x100).toString(16).substr(1));\n}\n\nfunction stringify(arr, offset = 0) {\n  // Note: Be careful editing this code!  It's been tuned for performance\n  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434\n  const uuid = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + '-' + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + '-' + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + '-' + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + '-' + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase(); // Consistency check for valid UUID.  If this throws, it's likely due to one\n  // of the following:\n  // - One or more input array values don't map to a hex octet (leading to\n  // \"undefined\" in the uuid)\n  // - Invalid input values for the RFC `version` or `variant` fields\n\n  if (!Object(_validate_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(uuid)) {\n    throw TypeError('Stringified UUID is invalid');\n  }\n\n  return uuid;\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (stringify);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/stringify.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/v1.js":
/*!***********************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/v1.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ \"./node_modules/uuid/dist/esm-node/rng.js\");\n/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stringify.js */ \"./node_modules/uuid/dist/esm-node/stringify.js\");\n\n // **`v1()` - Generate time-based UUID**\n//\n// Inspired by https://github.com/LiosK/UUID.js\n// and http://docs.python.org/library/uuid.html\n\nlet _nodeId;\n\nlet _clockseq; // Previous uuid creation time\n\n\nlet _lastMSecs = 0;\nlet _lastNSecs = 0; // See https://github.com/uuidjs/uuid for API details\n\nfunction v1(options, buf, offset) {\n  let i = buf && offset || 0;\n  const b = buf || new Array(16);\n  options = options || {};\n  let node = options.node || _nodeId;\n  let clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq; // node and clockseq need to be initialized to random values if they're not\n  // specified.  We do this lazily to minimize issues related to insufficient\n  // system entropy.  See #189\n\n  if (node == null || clockseq == null) {\n    const seedBytes = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n    if (node == null) {\n      // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)\n      node = _nodeId = [seedBytes[0] | 0x01, seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]];\n    }\n\n    if (clockseq == null) {\n      // Per 4.2.2, randomize (14 bit) clockseq\n      clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;\n    }\n  } // UUID timestamps are 100 nano-second units since the Gregorian epoch,\n  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so\n  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'\n  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.\n\n\n  let msecs = options.msecs !== undefined ? options.msecs : Date.now(); // Per 4.2.1.2, use count of uuid's generated during the current clock\n  // cycle to simulate higher resolution clock\n\n  let nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1; // Time since last uuid creation (in msecs)\n\n  const dt = msecs - _lastMSecs + (nsecs - _lastNSecs) / 10000; // Per 4.2.1.2, Bump clockseq on clock regression\n\n  if (dt < 0 && options.clockseq === undefined) {\n    clockseq = clockseq + 1 & 0x3fff;\n  } // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new\n  // time interval\n\n\n  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {\n    nsecs = 0;\n  } // Per 4.2.1.2 Throw error if too many uuids are requested\n\n\n  if (nsecs >= 10000) {\n    throw new Error(\"uuid.v1(): Can't create more than 10M uuids/sec\");\n  }\n\n  _lastMSecs = msecs;\n  _lastNSecs = nsecs;\n  _clockseq = clockseq; // Per 4.1.4 - Convert from unix epoch to Gregorian epoch\n\n  msecs += 12219292800000; // `time_low`\n\n  const tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;\n  b[i++] = tl >>> 24 & 0xff;\n  b[i++] = tl >>> 16 & 0xff;\n  b[i++] = tl >>> 8 & 0xff;\n  b[i++] = tl & 0xff; // `time_mid`\n\n  const tmh = msecs / 0x100000000 * 10000 & 0xfffffff;\n  b[i++] = tmh >>> 8 & 0xff;\n  b[i++] = tmh & 0xff; // `time_high_and_version`\n\n  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version\n\n  b[i++] = tmh >>> 16 & 0xff; // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)\n\n  b[i++] = clockseq >>> 8 | 0x80; // `clock_seq_low`\n\n  b[i++] = clockseq & 0xff; // `node`\n\n  for (let n = 0; n < 6; ++n) {\n    b[i + n] = node[n];\n  }\n\n  return buf || Object(_stringify_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(b);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (v1);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/v1.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/v3.js":
/*!***********************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/v3.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _v35_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v35.js */ \"./node_modules/uuid/dist/esm-node/v35.js\");\n/* harmony import */ var _md5_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./md5.js */ \"./node_modules/uuid/dist/esm-node/md5.js\");\n\n\nconst v3 = Object(_v35_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])('v3', 0x30, _md5_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (v3);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/v3.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/v35.js":
/*!************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/v35.js ***!
  \************************************************/
/*! exports provided: DNS, URL, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DNS\", function() { return DNS; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"URL\", function() { return URL; });\n/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./stringify.js */ \"./node_modules/uuid/dist/esm-node/stringify.js\");\n/* harmony import */ var _parse_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./parse.js */ \"./node_modules/uuid/dist/esm-node/parse.js\");\n\n\n\nfunction stringToBytes(str) {\n  str = unescape(encodeURIComponent(str)); // UTF8 escape\n\n  const bytes = [];\n\n  for (let i = 0; i < str.length; ++i) {\n    bytes.push(str.charCodeAt(i));\n  }\n\n  return bytes;\n}\n\nconst DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';\nconst URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (name, version, hashfunc) {\n  function generateUUID(value, namespace, buf, offset) {\n    if (typeof value === 'string') {\n      value = stringToBytes(value);\n    }\n\n    if (typeof namespace === 'string') {\n      namespace = Object(_parse_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(namespace);\n    }\n\n    if (namespace.length !== 16) {\n      throw TypeError('Namespace must be array-like (16 iterable integer values, 0-255)');\n    } // Compute hash of namespace and value, Per 4.3\n    // Future: Use spread syntax when supported on all platforms, e.g. `bytes =\n    // hashfunc([...namespace, ... value])`\n\n\n    let bytes = new Uint8Array(16 + value.length);\n    bytes.set(namespace);\n    bytes.set(value, namespace.length);\n    bytes = hashfunc(bytes);\n    bytes[6] = bytes[6] & 0x0f | version;\n    bytes[8] = bytes[8] & 0x3f | 0x80;\n\n    if (buf) {\n      offset = offset || 0;\n\n      for (let i = 0; i < 16; ++i) {\n        buf[offset + i] = bytes[i];\n      }\n\n      return buf;\n    }\n\n    return Object(_stringify_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(bytes);\n  } // Function#name is not settable on some platforms (#270)\n\n\n  try {\n    generateUUID.name = name; // eslint-disable-next-line no-empty\n  } catch (err) {} // For CommonJS default export support\n\n\n  generateUUID.DNS = DNS;\n  generateUUID.URL = URL;\n  return generateUUID;\n});\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/v35.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/v4.js":
/*!***********************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/v4.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ \"./node_modules/uuid/dist/esm-node/rng.js\");\n/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stringify.js */ \"./node_modules/uuid/dist/esm-node/stringify.js\");\n\n\n\nfunction v4(options, buf, offset) {\n  options = options || {};\n  const rnds = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`\n\n  rnds[6] = rnds[6] & 0x0f | 0x40;\n  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided\n\n  if (buf) {\n    offset = offset || 0;\n\n    for (let i = 0; i < 16; ++i) {\n      buf[offset + i] = rnds[i];\n    }\n\n    return buf;\n  }\n\n  return Object(_stringify_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(rnds);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (v4);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/v4.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/v5.js":
/*!***********************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/v5.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _v35_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v35.js */ \"./node_modules/uuid/dist/esm-node/v35.js\");\n/* harmony import */ var _sha1_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sha1.js */ \"./node_modules/uuid/dist/esm-node/sha1.js\");\n\n\nconst v5 = Object(_v35_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])('v5', 0x50, _sha1_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (v5);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/v5.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/validate.js":
/*!*****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/validate.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _regex_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./regex.js */ \"./node_modules/uuid/dist/esm-node/regex.js\");\n\n\nfunction validate(uuid) {\n  return typeof uuid === 'string' && _regex_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"].test(uuid);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (validate);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/validate.js?");

/***/ }),

/***/ "./node_modules/uuid/dist/esm-node/version.js":
/*!****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-node/version.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validate.js */ \"./node_modules/uuid/dist/esm-node/validate.js\");\n\n\nfunction version(uuid) {\n  if (!Object(_validate_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(uuid)) {\n    throw TypeError('Invalid UUID');\n  }\n\n  return parseInt(uuid.substr(14, 1), 16);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (version);\n\n//# sourceURL=webpack:///./node_modules/uuid/dist/esm-node/version.js?");

/***/ }),

/***/ "./src/classes/events.ts":
/*!*******************************!*\
  !*** ./src/classes/events.ts ***!
  \*******************************/
/*! exports provided: Eventable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Eventable\", function() { return Eventable; });\nclass Eventable {\n    constructor() {\n        this._listeners = {};\n    }\n    listen(eventName, callback) {\n        if (!this._listeners[eventName])\n            this._listeners[eventName] = [];\n        this._listeners[eventName].push(callback);\n    }\n    _trigger(eventName, ...args) {\n        if (this._listeners[eventName]) {\n            this._listeners[eventName].forEach(listener => {\n                listener(...args);\n            });\n        }\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/events.ts?");

/***/ }),

/***/ "./src/classes/math.ts":
/*!*****************************!*\
  !*** ./src/classes/math.ts ***!
  \*****************************/
/*! exports provided: Vector */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Vector\", function() { return Vector; });\nclass Vector {\n    constructor(x, y) {\n        this.x = x;\n        this.y = y;\n    }\n    static fromMatter(v) {\n        return new Vector(v.x, v.y);\n    }\n    setX(x) {\n        return new Vector(x, this.y);\n    }\n    setY(y) {\n        return new Vector(this.x, y);\n    }\n    module() {\n        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));\n    }\n    normalize() {\n        const module = this.module();\n        if (module > 0)\n            return new Vector(this.x / module, this.y / module);\n        return this;\n    }\n    add(v) {\n        return new Vector(this.x + v.x, this.y + v.y);\n    }\n    substract(v) {\n        return new Vector(this.x - v.x, this.y - v.y);\n    }\n    multiply(n) {\n        return new Vector(this.x * n, this.y * n);\n    }\n    invert() {\n        return this.multiply(-1);\n    }\n    director() {\n        return new Vector(this.x !== 0 ? this.x / Math.abs(this.x) : 0, this.y !== 0 ? this.y / Math.abs(this.y) : 0);\n    }\n    dot(v) {\n        return this.x * v.x + this.y * v.y;\n    }\n    angle(v) {\n        if (v)\n            return Math.acos(this.dot(v) / (this.module() * v.module()));\n        if (this.x === 0 && this.y > 0)\n            return Math.PI / 2;\n        if (this.x === 0 && this.y < 0)\n            return -Math.PI / 2;\n        return Math.atan(this.y / this.x) - (this.x < 0 ? Math.PI : 0);\n    }\n    rotate(angle) {\n        return new Vector(this.x * Math.cos(angle) - this.y * Math.sin(angle), this.x * Math.sin(angle) - this.y * Math.cos(angle));\n    }\n    copy() {\n        return new Vector(this.x, this.y);\n    }\n    isEqual(v) {\n        return (this.x === v.x && this.y === v.y);\n    }\n    toArray() {\n        return [this.x, this.y];\n    }\n    serialize() {\n        return {\n            x: this.x,\n            y: this.y\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/math.ts?");

/***/ }),

/***/ "./src/classes/objects/booster.ts":
/*!****************************************!*\
  !*** ./src/classes/objects/booster.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Booster; });\n/* harmony import */ var _physics__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../physics */ \"./src/classes/physics.ts\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../globals */ \"./src/globals.ts\");\n/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories */ \"./src/classes/objects/categories.ts\");\n\n\n\nclass Booster extends _physics__WEBPACK_IMPORTED_MODULE_0__[\"Collideable\"] {\n    constructor(position) {\n        super(position);\n        this._handleCollision = (body) => {\n            this.dematerialize();\n        };\n        this._body = new _physics__WEBPACK_IMPORTED_MODULE_0__[\"CircleCollideable\"](9999, position, _globals__WEBPACK_IMPORTED_MODULE_1__[\"boosterRadius\"], {\n            mass: 9999,\n            inertia: 9999,\n            friction: 0,\n            frictionAir: 0,\n            frictionStatic: 0,\n            isSensor: true,\n            collisionFilter: {\n                category: _categories__WEBPACK_IMPORTED_MODULE_2__[\"BoosterCategory\"]\n            },\n            plugin: {\n                duration: 10000,\n                onCollision: this._handleCollision\n            }\n        })._body;\n    }\n    serialize() {\n        return {\n            type: 'booster',\n            position: this.getPosition().serialize(),\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/objects/booster.ts?");

/***/ }),

/***/ "./src/classes/objects/categories.ts":
/*!*******************************************!*\
  !*** ./src/classes/objects/categories.ts ***!
  \*******************************************/
/*! exports provided: WallCategory, EndCategory, PlayerCategory, RocketCategory, BoosterCategory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"WallCategory\", function() { return WallCategory; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"EndCategory\", function() { return EndCategory; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PlayerCategory\", function() { return PlayerCategory; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RocketCategory\", function() { return RocketCategory; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BoosterCategory\", function() { return BoosterCategory; });\nconst WallCategory = 1;\nconst EndCategory = 2;\nconst PlayerCategory = 4;\nconst RocketCategory = 8;\nconst BoosterCategory = 16;\n\n\n//# sourceURL=webpack:///./src/classes/objects/categories.ts?");

/***/ }),

/***/ "./src/classes/objects/end.ts":
/*!************************************!*\
  !*** ./src/classes/objects/end.ts ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return End; });\n/* harmony import */ var _physics__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../physics */ \"./src/classes/physics.ts\");\n/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../math */ \"./src/classes/math.ts\");\n/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories */ \"./src/classes/objects/categories.ts\");\n\n\n\nclass End extends _physics__WEBPACK_IMPORTED_MODULE_0__[\"RectCollideable\"] {\n    constructor(position, width, height, options) {\n        super(0, new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](position.x + width / 2, position.y + height / 2), width, height, 0, Object.assign(Object.assign({ isStatic: true, isSensor: true }, (options ? options : {})), { collisionFilter: Object.assign({ category: _categories__WEBPACK_IMPORTED_MODULE_2__[\"EndCategory\"] }, (options && options.collisionFilter ? options.collisionFilter : {})) }));\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/objects/end.ts?");

/***/ }),

/***/ "./src/classes/objects/player.ts":
/*!***************************************!*\
  !*** ./src/classes/objects/player.ts ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Player; });\n/* harmony import */ var _physics__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../physics */ \"./src/classes/physics.ts\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../globals */ \"./src/globals.ts\");\n/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../math */ \"./src/classes/math.ts\");\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! matter-js */ \"matter-js\");\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(matter_js__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./categories */ \"./src/classes/objects/categories.ts\");\n\n\n\n\n\nclass Player extends _physics__WEBPACK_IMPORTED_MODULE_0__[\"Collideable\"] {\n    constructor(id, name, position) {\n        super(position);\n        this._body = new _physics__WEBPACK_IMPORTED_MODULE_0__[\"CircleCollideable\"](10, position, _globals__WEBPACK_IMPORTED_MODULE_1__[\"playerRadius\"], {\n            mass: 10,\n            restitution: 0.2,\n            frictionStatic: 0,\n            friction: 0.5,\n            frictionAir: 0.07,\n            collisionFilter: {\n                category: _categories__WEBPACK_IMPORTED_MODULE_4__[\"PlayerCategory\"]\n            }\n        })._body;\n        // this._body.plugin.owner = this;\n        this._pointingVector = new _math__WEBPACK_IMPORTED_MODULE_2__[\"Vector\"](0, -1);\n        this._direction = new _math__WEBPACK_IMPORTED_MODULE_2__[\"Vector\"](0, 0);\n        this._initialPosition = position;\n        this._id = id;\n        this._name = name;\n        this._acceleration = 0.07;\n        this._body.plugin.owner = this;\n        this._exploded = false;\n        this._life = 100;\n        this._boosted = false;\n        this._rockets = 5;\n        this._rocketCooldown = _globals__WEBPACK_IMPORTED_MODULE_1__[\"ROCKET_COOLDOWN\"];\n    }\n    changeDirection(direction) {\n        this._direction = direction;\n        if (direction.x !== 0 || direction.y !== 0) {\n            this._pointingVector = direction;\n        }\n    }\n    update(dt) {\n        if (this._direction.module()) {\n            const forcePosition = this._body.position;\n            const force = this._direction.normalize().multiply(this._acceleration);\n            matter_js__WEBPACK_IMPORTED_MODULE_3__[\"Body\"].applyForce(this._body, forcePosition, force);\n        }\n        this._updateRockets(dt);\n        super.update(dt);\n    }\n    _updateRockets(dt) {\n        if (this._rockets < _globals__WEBPACK_IMPORTED_MODULE_1__[\"MAX_ROCKETS\"]) {\n            this._rocketCooldown -= dt / 1000;\n            if (this._rocketCooldown <= 0) {\n                this._rockets += 1;\n                this._rocketCooldown = _globals__WEBPACK_IMPORTED_MODULE_1__[\"ROCKET_COOLDOWN\"];\n            }\n        }\n    }\n    shoot() {\n        if (this._rockets > 0) {\n            this._rockets--;\n            return true;\n        }\n        return false;\n    }\n    hit() {\n        this._life -= 25;\n        if (this._life <= 0)\n            this._exploded = true;\n        if (this._exploded) {\n            setTimeout(() => {\n                this.setPosition(this._initialPosition);\n                this._exploded = false;\n                this._life = 100;\n            }, 2000);\n        }\n    }\n    boost() {\n        this._acceleration = 0.14;\n        this._boosted = true;\n        if (this._boostTimeout)\n            clearTimeout(this._boostTimeout);\n        this._boostTimeout = setTimeout(() => {\n            this._acceleration = 0.07;\n            this._boosted = false;\n        }, 3000);\n    }\n    serialize() {\n        return {\n            type: 'player',\n            id: this._id,\n            name: this._name,\n            exploded: this._exploded,\n            angle: 0,\n            position: this.getPosition().serialize(),\n            direction: this._direction.serialize(),\n            life: this._life,\n            boosted: this._boosted,\n            rockets: this._rockets,\n            rocketCooldown: Math.round(this._rocketCooldown * 100 / _globals__WEBPACK_IMPORTED_MODULE_1__[\"ROCKET_COOLDOWN\"])\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/objects/player.ts?");

/***/ }),

/***/ "./src/classes/objects/rocket.ts":
/*!***************************************!*\
  !*** ./src/classes/objects/rocket.ts ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Rocket; });\n/* harmony import */ var _physics__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../physics */ \"./src/classes/physics.ts\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../globals */ \"./src/globals.ts\");\n/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories */ \"./src/classes/objects/categories.ts\");\n\n\n\nclass Rocket extends _physics__WEBPACK_IMPORTED_MODULE_0__[\"Collideable\"] {\n    constructor(owner) {\n        super(owner.getPosition());\n        this._handleCollision = (body) => {\n            this.dematerialize();\n        };\n        this._body = new _physics__WEBPACK_IMPORTED_MODULE_0__[\"CircleCollideable\"](9999, owner.getPosition(), _globals__WEBPACK_IMPORTED_MODULE_1__[\"playerRadius\"], {\n            mass: 9999,\n            inertia: 9999,\n            friction: 0,\n            frictionAir: 0,\n            frictionStatic: 0,\n            isSensor: true,\n            collisionFilter: {\n                category: _categories__WEBPACK_IMPORTED_MODULE_2__[\"RocketCategory\"]\n            },\n            plugin: {\n                owner: owner,\n                duration: 2500,\n                onCollision: this._handleCollision\n            }\n        })._body;\n        this.setVelocity(owner.getVelocity().add(owner._pointingVector.normalize().multiply(20)));\n        this._owner = owner;\n    }\n    serialize() {\n        return {\n            type: 'rocket',\n            angle: this.getAngle(),\n            position: this.getPosition().serialize(),\n            direction: this.getVelocity().normalize()\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/objects/rocket.ts?");

/***/ }),

/***/ "./src/classes/objects/wall.ts":
/*!*************************************!*\
  !*** ./src/classes/objects/wall.ts ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Wall; });\n/* harmony import */ var _physics__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../physics */ \"./src/classes/physics.ts\");\n/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../math */ \"./src/classes/math.ts\");\n/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories */ \"./src/classes/objects/categories.ts\");\n\n\n\nclass Wall extends _physics__WEBPACK_IMPORTED_MODULE_0__[\"RectCollideable\"] {\n    constructor(position, width, height, options) {\n        super(0, new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](position.x + width / 2, position.y + height / 2), width, height, 0, Object.assign(Object.assign({ isStatic: true }, (options ? options : {})), { collisionFilter: Object.assign({ category: _categories__WEBPACK_IMPORTED_MODULE_2__[\"WallCategory\"] }, (options && options.collisionFilter ? options.collisionFilter : {})) }));\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/objects/wall.ts?");

/***/ }),

/***/ "./src/classes/physics.ts":
/*!********************************!*\
  !*** ./src/classes/physics.ts ***!
  \********************************/
/*! exports provided: Collideable, CircleCollideable, RectCollideable, PolygonCollideable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Collideable\", function() { return Collideable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CircleCollideable\", function() { return CircleCollideable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RectCollideable\", function() { return RectCollideable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PolygonCollideable\", function() { return PolygonCollideable; });\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! matter-js */ \"matter-js\");\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(matter_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./math */ \"./src/classes/math.ts\");\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ \"./src/utils.ts\");\n/* harmony import */ var _events__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./events */ \"./src/classes/events.ts\");\n/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! uuid */ \"./node_modules/uuid/dist/esm-node/index.js\");\n\n\n\n\n\nclass Collideable extends _events__WEBPACK_IMPORTED_MODULE_3__[\"Eventable\"] {\n    // eslint-disable-next-line @typescript-eslint/no-unused-vars\n    constructor(_position) {\n        super();\n        this._mounted = false;\n        this.isExpired = () => {\n            const { duration } = this._body.plugin;\n            if (duration && this._mounted && this._mountedDT && Object(_utils__WEBPACK_IMPORTED_MODULE_2__[\"elapsedSeconds\"])(this._mountedDT) * 1000 > duration)\n                return true;\n            return false;\n        };\n        this._id = Object(uuid__WEBPACK_IMPORTED_MODULE_4__[\"v4\"])();\n        this._body = matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Bodies\"].circle(0, 0, 0, {});\n    }\n    materialize(world) {\n        if (!this._mounted) {\n            this._world = world;\n            this._mounted = true;\n            this._mountedDT = new Date();\n            matter_js__WEBPACK_IMPORTED_MODULE_0__[\"World\"].add(world, this._body);\n            this._trigger('materialize');\n        }\n    }\n    dematerialize() {\n        if (this._mounted && this._world) {\n            this._mounted = false;\n            matter_js__WEBPACK_IMPORTED_MODULE_0__[\"World\"].remove(this._world, this._body);\n            this._trigger('dematerialize');\n        }\n    }\n    setVelocity(velocity, angularVelocity = 0) {\n        if (this._body) {\n            matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Body\"].setVelocity(this._body, velocity);\n            matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Body\"].setAngularVelocity(this._body, angularVelocity);\n        }\n    }\n    getVelocity() {\n        return _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"].fromMatter(this._body.velocity);\n    }\n    setPosition(position) {\n        matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Body\"].setPosition(this._body, position);\n    }\n    getAngle() {\n        return this._body.angle;\n    }\n    setAngle(angle) {\n        matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Body\"].setAngle(this._body, angle);\n    }\n    getPosition() {\n        return _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"].fromMatter(this._body.position);\n    }\n    // eslint-disable-next-line @typescript-eslint/no-unused-vars\n    update(_dt) {\n        if (this._body.speed > 0 && this._body.speed < 0.01) {\n            this.setVelocity(new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](0, 0));\n        }\n    }\n    getPoints() {\n        if (this._body.parts.length > 1)\n            return this._body.parts.map(b => _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"].fromMatter(b.position));\n        return this._body.vertices.map(v => _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"].fromMatter(v));\n    }\n    applyForce(force, position) {\n        matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Body\"].applyForce(this._body, position, force);\n    }\n    serialize() {\n        return {\n            type: 'none',\n            position: this.getPosition().serialize(),\n            angle: this.getAngle(),\n            direction: this.getVelocity().director()\n        };\n    }\n}\nclass CircleCollideable extends Collideable {\n    constructor(mass, position, radius, options) {\n        super(position);\n        this._body = matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Bodies\"].circle(position.x, position.y, radius, Object.assign({ plugin: {\n                owner: this,\n            }, mass }, (options ? options : {})));\n    }\n    serialize() {\n        return {\n            type: 'circle',\n            position: this.getPosition().serialize(),\n            angle: this.getAngle(),\n            radius: this._body.circleRadius || 0,\n            direction: this.getVelocity().director()\n        };\n    }\n}\nclass RectCollideable extends Collideable {\n    constructor(mass, position, width, height, angle = 0, options) {\n        super(position);\n        this._width = width;\n        this._height = height;\n        this._body = matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Bodies\"].rectangle(position.x, position.y, width, height, Object.assign({ plugin: {\n                owner: this,\n            }, mass,\n            angle }, (options ? options : {})));\n    }\n    serialize() {\n        return {\n            type: 'rect',\n            position: this.getPosition().serialize(),\n            angle: this.getAngle(),\n            width: this._width,\n            height: this._height,\n            direction: this.getVelocity().director()\n        };\n    }\n}\nclass PolygonCollideable extends Collideable {\n    constructor(mass, position, points, options) {\n        super(position);\n        this._body = matter_js__WEBPACK_IMPORTED_MODULE_0__[\"Bodies\"].fromVertices(position.x, position.y, points, Object.assign({ plugin: {\n                owner: this,\n            }, mass }, (options ? options : {})));\n    }\n    serialize() {\n        return {\n            type: 'polygon',\n            position: this.getPosition().serialize(),\n            angle: this.getAngle(),\n            points: this.getPoints(),\n            direction: this.getVelocity().director()\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/physics.ts?");

/***/ }),

/***/ "./src/classes/room.ts":
/*!*****************************!*\
  !*** ./src/classes/room.ts ***!
  \*****************************/
/*! exports provided: Room */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Room\", function() { return Room; });\n/* harmony import */ var _objects_player__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./objects/player */ \"./src/classes/objects/player.ts\");\n/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./math */ \"./src/classes/math.ts\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../globals */ \"./src/globals.ts\");\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! matter-js */ \"matter-js\");\n/* harmony import */ var matter_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(matter_js__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _objects_wall__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./objects/wall */ \"./src/classes/objects/wall.ts\");\n/* harmony import */ var _objects_end__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./objects/end */ \"./src/classes/objects/end.ts\");\n/* harmony import */ var _objects_booster__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./objects/booster */ \"./src/classes/objects/booster.ts\");\n/* harmony import */ var _objects_categories__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./objects/categories */ \"./src/classes/objects/categories.ts\");\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils */ \"./src/utils.ts\");\n/* harmony import */ var _objects_rocket__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./objects/rocket */ \"./src/classes/objects/rocket.ts\");\nvar __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\n    return new (P || (P = Promise))(function (resolve, reject) {\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\n    });\n};\n\n\n\n\n\n\n\n\n\n\nclass Room {\n    constructor(id, name, socket) {\n        this._effects = [];\n        this._countdown = 0;\n        this._resetCountdown = 10;\n        this._playing = false;\n        this._walls = [\n            new _objects_wall__WEBPACK_IMPORTED_MODULE_4__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](0, -100), _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, 100),\n            new _objects_wall__WEBPACK_IMPORTED_MODULE_4__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](0, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y), _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, 100),\n            new _objects_wall__WEBPACK_IMPORTED_MODULE_4__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](-100, 0), 100, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y),\n            new _objects_wall__WEBPACK_IMPORTED_MODULE_4__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](_globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, 0), 100, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y),\n        ];\n        this._startWall = new _objects_wall__WEBPACK_IMPORTED_MODULE_4__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](0, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y - 500), _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, 100);\n        this._endSensor = new _objects_end__WEBPACK_IMPORTED_MODULE_5__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](0, 500), _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, 100);\n        //socket BEGIN\n        this._emit = () => {\n            this._socket.emit('update', this.serialize());\n        };\n        this._handleSocket = (socket) => {\n            // eslint-disable-next-line @typescript-eslint/no-this-alias\n            const self = this;\n            socket.on('connection', function (clientSocket) {\n                const playerId = clientSocket.id;\n                const { name } = clientSocket.handshake.query;\n                const player = self._addPlayer(playerId, name);\n                clientSocket.emit('login_success', Object.assign({ self: player.serialize() }, self.serialize()));\n                clientSocket.broadcast.emit('player_join', player.serialize());\n                // eslint-disable-next-line @typescript-eslint/no-unused-vars\n                clientSocket.on('disconnect', function (_) {\n                    self._removePlayer(playerId);\n                    clientSocket.broadcast.emit('player_leave', { id: playerId });\n                });\n                clientSocket.on('request_direction_change', function (payload) {\n                    self._playerDirectionChanged(playerId, payload.direction);\n                });\n                clientSocket.on('request_key_press', function (payload) {\n                    self._playerKeyPress(playerId, payload.keyCode);\n                });\n                clientSocket.on('request_send_message', function (payload) {\n                    socket.emit('player_message', { name: player._name, message: payload.message });\n                });\n                if (self._world)\n                    player.materialize(self._world);\n            });\n        };\n        //players END\n        //engine BEGIN\n        this._initializeEngine = () => {\n            this._engine = matter_js__WEBPACK_IMPORTED_MODULE_3__[\"Engine\"].create();\n            this._world = this._engine.world;\n            this._world.gravity.x = 0;\n            this._world.gravity.y = 0;\n            matter_js__WEBPACK_IMPORTED_MODULE_3__[\"Events\"].on(this._engine, 'collisionStart', this._handleCollisionsStart);\n            matter_js__WEBPACK_IMPORTED_MODULE_3__[\"Events\"].on(this._engine, 'collisionEnd', this._handleCollisionsEnd);\n            let i = 0;\n            this._interval = setInterval(() => {\n                i++;\n                if (this._update() || (1000 / _globals__WEBPACK_IMPORTED_MODULE_2__[\"timeConstant\"]) <= i) {\n                    i = 0;\n                    this._emit();\n                }\n            }, _globals__WEBPACK_IMPORTED_MODULE_2__[\"timeConstant\"]);\n            this._mountWalls();\n            this._mountSensors();\n        };\n        this._handleCollisionsStart = (e) => {\n            const pairs = e.pairs;\n            for (let i = 0, j = pairs.length; i != j; ++i) {\n                const pair = pairs[i];\n                this._handleGameCollision(pair.bodyA, pair.bodyB);\n                this._handleGameCollision(pair.bodyB, pair.bodyA);\n            }\n        };\n        this._handleCollisionsEnd = (e) => {\n            const pairs = e.pairs;\n            for (let i = 0, j = pairs.length; i != j; ++i) {\n                // const pair = pairs[i];\n            }\n        };\n        //physics END\n        //game BEGIN\n        this._placements = [];\n        this._mountWalls = () => {\n            this._walls.forEach(wall => {\n                if (this._world)\n                    wall.materialize(this._world);\n            });\n            if (this._world)\n                this._startWall.materialize(this._world);\n        };\n        this._generateBooster = () => {\n            const booster = new _objects_booster__WEBPACK_IMPORTED_MODULE_6__[\"default\"](new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](_globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x * Math.random(), _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y * Math.random()));\n            booster.listen('dematerialize', () => {\n                this._effects = this._effects.filter(effect => effect._id !== booster._id);\n            });\n            this._effects.push(booster);\n            if (this._world)\n                booster.materialize(this._world);\n        };\n        this._mountStartWalls = () => {\n            console.log('_mountStartWalls');\n            if (this._world)\n                this._startWall.materialize(this._world);\n        };\n        this._unmountStartWalls = () => {\n            console.log('_unmountStartWalls');\n            if (this._world)\n                this._startWall.dematerialize();\n        };\n        this._mountSensors = () => {\n            if (this._world)\n                this._endSensor.materialize(this._world);\n        };\n        this._handleGameCollision = (bodyA, bodyB) => {\n            var _a;\n            if (bodyA.isSensor && bodyA.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"EndCategory\"] && bodyB.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"PlayerCategory\"]) {\n                if (bodyB.plugin.owner) {\n                    this._playerEndRace(bodyB.plugin.owner);\n                }\n            }\n            if (bodyA.isSensor && bodyA.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"RocketCategory\"] && bodyB.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"PlayerCategory\"]) {\n                if (bodyA.plugin.owner && bodyB.plugin.owner && bodyA.plugin.owner._id !== bodyB.plugin.owner._id) {\n                    console.log(\"hit\");\n                    bodyB.plugin.owner.hit();\n                    if (bodyA.plugin.onCollision)\n                        bodyA.plugin.onCollision(bodyB);\n                }\n            }\n            if (bodyA.isSensor && bodyA.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"BoosterCategory\"] && bodyB.collisionFilter.category === _objects_categories__WEBPACK_IMPORTED_MODULE_7__[\"PlayerCategory\"]) {\n                console.log(\"boosted\");\n                (_a = bodyB.plugin.owner) === null || _a === void 0 ? void 0 : _a.boost();\n                if (bodyA.plugin.onCollision)\n                    bodyA.plugin.onCollision(bodyB);\n            }\n        };\n        this._playerEndRace = (player) => {\n            if (!this._placements.some(x => x._id === player._id)) {\n                this._placements.push(player);\n                player._acceleration = 0;\n            }\n            if (this._placements.length === this._connectedPlayers().length) {\n                this._reset();\n            }\n        };\n        this._begin = () => {\n            // eslint-disable-next-line @typescript-eslint/no-this-alias\n            const self = this;\n            this._startDate = new Date();\n            setTimeout(() => {\n                console.log(\"_countdown\");\n                this._playing = true;\n                self._startDate = undefined;\n                self._endDate = undefined;\n                self._unmountStartWalls();\n            }, this._countdown * 1000);\n        };\n        this._reset = () => {\n            // eslint-disable-next-line @typescript-eslint/no-this-alias\n            const self = this;\n            this._endDate = new Date();\n            setTimeout(() => {\n                self._restore();\n                if (this._connectedPlayers().length > 0) {\n                    self._begin();\n                }\n            }, this._resetCountdown * 1000);\n        };\n        this._restore = () => {\n            this._playing = false;\n            this._startDate = undefined;\n            this._endDate = undefined;\n            this._placements = [];\n            this._effects.forEach(effect => {\n                effect.dematerialize();\n            });\n            this._effects = [];\n            this._connectedPlayers().map(player => {\n                player.setPosition(new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](Math.random() * _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y - Math.random() * 100 - 50));\n                player._acceleration = 0.07;\n            });\n            this._mountStartWalls();\n        };\n        this._id = id;\n        this._name = name;\n        this._players = {};\n        this._socket = socket;\n        this._initializeEngine();\n        this._handleSocket(socket);\n    }\n    //socket END\n    //players BEGIN\n    _connectedPlayers() {\n        return Object.keys(this._players).reduce((r, key) => {\n            if (this._socket.clients(() => true).connected[key]) {\n                r.push(this._players[key]);\n            }\n            else {\n                delete this._players[key];\n            }\n            return r;\n        }, []);\n    }\n    _addPlayer(id, name) {\n        if (Object.entries(this._players).length === 0)\n            this._begin();\n        this._players[id] = new _objects_player__WEBPACK_IMPORTED_MODULE_0__[\"default\"](id, name, _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].setX(Math.random() * _globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].x).setY(_globals__WEBPACK_IMPORTED_MODULE_2__[\"mapSize\"].y - Math.random() * 100 - 50));\n        return this._players[id];\n    }\n    _removePlayer(id) {\n        if (this._players[id]) {\n            this._players[id].dematerialize();\n        }\n        this._players = Object.keys(this._players).reduce((result, key) => {\n            if (key !== id) {\n                result[key] = this._players[key];\n            }\n            else {\n                delete this._players[key];\n            }\n            return result;\n        }, {});\n        if (this._connectedPlayers().length === 0)\n            this._restore();\n    }\n    _playerDirectionChanged(id, { x, y }) {\n        if (this._players[id]) {\n            this._players[id].changeDirection(new _math__WEBPACK_IMPORTED_MODULE_1__[\"Vector\"](x, y));\n        }\n    }\n    _playerKeyPress(id, code) {\n        return __awaiter(this, void 0, void 0, function* () {\n            if (this._playing) {\n                if (code === 'Space') {\n                    const player = this._players[id];\n                    if (player.shoot()) {\n                        const rocket = new _objects_rocket__WEBPACK_IMPORTED_MODULE_9__[\"default\"](player);\n                        rocket.listen('dematerialize', () => {\n                            this._effects = this._effects.filter(effect => effect._id !== rocket._id);\n                        });\n                        this._effects.push(rocket);\n                        if (this._world)\n                            rocket.materialize(this._world);\n                    }\n                }\n            }\n        });\n    }\n    _update() {\n        if (this._engine && this._world) {\n            matter_js__WEBPACK_IMPORTED_MODULE_3__[\"Engine\"].update(this._engine);\n            this._connectedPlayers().forEach(player => player.update(_globals__WEBPACK_IMPORTED_MODULE_2__[\"timeConstant\"]));\n            if (this._playing && Math.random() > 0.9)\n                this._generateBooster();\n            this._effects = this._effects.reduce((r, effect) => {\n                if (effect.isExpired())\n                    effect.dematerialize();\n                else {\n                    effect.update(_globals__WEBPACK_IMPORTED_MODULE_2__[\"timeConstant\"]);\n                    r.push(effect);\n                }\n                return r;\n            }, []);\n            return true; //this._world.bodies.some(x => x.speed > 0);\n        }\n        return false;\n    }\n    //game END\n    _getCountdown() {\n        if (this._endDate) {\n            return Math.round(this._resetCountdown - Object(_utils__WEBPACK_IMPORTED_MODULE_8__[\"elapsedSeconds\"])(this._endDate, new Date()));\n        }\n        else if (this._startDate) {\n            return Math.round(this._countdown - Object(_utils__WEBPACK_IMPORTED_MODULE_8__[\"elapsedSeconds\"])(this._startDate, new Date()));\n        }\n        return 0;\n    }\n    serialize() {\n        return {\n            players: Object.entries(this._players)\n                .reduce((result, [key, player]) => {\n                result[key] = player.serialize();\n                return result;\n            }, {}),\n            effects: this._effects.map(effect => effect.serialize()),\n            placements: this._placements.map(player => player.serialize()),\n            countdown: this._getCountdown(),\n            gameEnded: this._endDate ? true : false,\n        };\n    }\n}\n\n\n//# sourceURL=webpack:///./src/classes/room.ts?");

/***/ }),

/***/ "./src/globals.ts":
/*!************************!*\
  !*** ./src/globals.ts ***!
  \************************/
/*! exports provided: DEBUG, timeConstant, playerRadius, boosterRadius, mapSize, ROCKET_COOLDOWN, MAX_ROCKETS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DEBUG\", function() { return DEBUG; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"timeConstant\", function() { return timeConstant; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"playerRadius\", function() { return playerRadius; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"boosterRadius\", function() { return boosterRadius; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"mapSize\", function() { return mapSize; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ROCKET_COOLDOWN\", function() { return ROCKET_COOLDOWN; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MAX_ROCKETS\", function() { return MAX_ROCKETS; });\n/* harmony import */ var _classes_math__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/math */ \"./src/classes/math.ts\");\n\nconst DEBUG = false;\nconst timeConstant = 25;\nconst playerRadius = 40;\nconst boosterRadius = 30;\nconst mapSize = new _classes_math__WEBPACK_IMPORTED_MODULE_0__[\"Vector\"](2000, 50000);\nconst ROCKET_COOLDOWN = 1.5;\nconst MAX_ROCKETS = 5;\n\n\n//# sourceURL=webpack:///./src/globals.ts?");

/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var socket_io__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! socket.io */ \"socket.io\");\n/* harmony import */ var socket_io__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(socket_io__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _classes_room__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./classes/room */ \"./src/classes/room.ts\");\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! http */ \"http\");\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nconst ENV_DEVELOPMENT = \"development\" === \"development\";\nconsole.log(ENV_DEVELOPMENT ? \"DEVELOPMENT ENVIRONMENT\" : \"PRODUCTION ENVIRONMENT\");\nconst app = express__WEBPACK_IMPORTED_MODULE_0__();\nconst http = new http__WEBPACK_IMPORTED_MODULE_3__[\"Server\"](app);\nconst io = socket_io__WEBPACK_IMPORTED_MODULE_1__(http, { path: '/ws' });\nconst roomId = '/always-open';\n// eslint-disable-next-line @typescript-eslint/no-unused-vars\nconst matches = {\n    [roomId]: new _classes_room__WEBPACK_IMPORTED_MODULE_2__[\"Room\"](roomId, \"Always open\", io.of(roomId))\n};\nhttp.listen(3000, function () {\n    console.log('started on port 3000');\n    process.on(\"SIGINT\", closeApp);\n    process.on(\"SIGTERM\", closeApp);\n});\nfunction closeApp() {\n    process.exit(0);\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (app);\n\n\n//# sourceURL=webpack:///./src/index.ts?");

/***/ }),

/***/ "./src/utils.ts":
/*!**********************!*\
  !*** ./src/utils.ts ***!
  \**********************/
/*! exports provided: reverseArray, elapsedSeconds, round */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"reverseArray\", function() { return reverseArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"elapsedSeconds\", function() { return elapsedSeconds; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"round\", function() { return round; });\nconst roundingDecimals = 3;\nconst reverseArray = (arr) => {\n    return arr.reduce((result, e, i) => {\n        result[arr.length - i - 1] = e;\n        return result;\n    }, []);\n};\nconst elapsedSeconds = (from, to) => {\n    if (!to) {\n        return (new Date().getTime() - from.getTime()) / 1000;\n    }\n    return (to.getTime() - from.getTime()) / 1000;\n};\nconst round = (n) => Math.round(n * roundingDecimals) / roundingDecimals;\n\n\n//# sourceURL=webpack:///./src/utils.ts?");

/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"crypto\");\n\n//# sourceURL=webpack:///external_%22crypto%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack:///external_%22http%22?");

/***/ }),

/***/ "matter-js":
/*!****************************!*\
  !*** external "matter-js" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"matter-js\");\n\n//# sourceURL=webpack:///external_%22matter-js%22?");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"socket.io\");\n\n//# sourceURL=webpack:///external_%22socket.io%22?");

/***/ })

/******/ });
# DEV Notes

##### Folders
* ./api: Main Django API, controls the querying of features, etc
* ./client: UI
* ./nginx: Will serve the files in prod

### How to run

1. Execute docker-compose up using .dev.yml if you are in mac/linux and .windows.yml if you are in windows and let the containers build.

* ui: http://localhost:8000/
* api: http://localhost:8080/
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ApplicationContextProvider } from './store';

import './styles.scss';
import { World } from './views';

function App() {
  return (
    <BrowserRouter basename="">
      <ApplicationContextProvider>
        <div className="App">
          <Switch>
            <Route exact path={'/'} component={World} />
          </Switch>
        </div>
      </ApplicationContextProvider>
    </BrowserRouter>
  );
}

export default App;

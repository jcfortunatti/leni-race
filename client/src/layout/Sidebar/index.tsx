import React, { ReactElement } from 'react';
import { DefaultComponentProps } from '../../store/types';

const Sidebar = (props: DefaultComponentProps): ReactElement => {
  return <div className="sidebar">{props.children}</div>;
};

export default Sidebar;

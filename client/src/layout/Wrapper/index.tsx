import React, { ReactElement } from 'react';
import { DefaultComponentProps } from '../../store/types';

const Wrapper = (props: DefaultComponentProps): ReactElement => {
  return <div className="wrapper">{props.children}</div>;
};

export default Wrapper;

import React, { ReactElement } from 'react';
import { DefaultComponentProps } from '../../store/types';

const Container = (props: DefaultComponentProps): ReactElement => {
  return <div className="container">{props.children}</div>;
};

export default Container;

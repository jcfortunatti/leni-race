import React, { useContext, useState, ReactElement } from 'react';
import { ApplicationContext } from '../../../store';
import { REQUEST_SEND_MESSAGE } from '../../../store/constants';
import { Input } from './Input';
import { Button } from './Button';
import { Message } from './Message';
import './styles.scss';

interface IChatProps {
  width?: number;
  height?: number;
  onFocus?: () => void;
  onBlur?: () => void;
}

const Chat = (props: IChatProps): ReactElement => {
  const context = useContext(ApplicationContext);
  const [message, setMessage] = useState('');

  const sendMessage = () => {
    if (message.length) {
      context[REQUEST_SEND_MESSAGE](message);
      setMessage('');
    }
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;
    if (keyCode === 13) {
      e.preventDefault();
      sendMessage();
    }
  };

  const handleChange = (value: string) => {
    setMessage(value);
  };

  const handleFocus = () => {
    const { onFocus } = props;
    if (onFocus) onFocus();
  };

  const handleBlur = () => {
    const { onBlur } = props;
    if (onBlur) onBlur();
  };

  return (
    <foreignObject y={props.height - 200} width={300} height={200}>
      <div className="game-chat">
        <div>
          {context.messages.map((msg, i) => (
            <Message key={i} {...msg} />
          ))}
        </div>
        <div>
          <Input
            value={message}
            onBlur={handleBlur}
            onFocus={handleFocus}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
          />
          <Button onClick={sendMessage} />
        </div>
      </div>
    </foreignObject>
  );
};

export default Chat;

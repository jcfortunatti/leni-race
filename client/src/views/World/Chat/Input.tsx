import React, { ReactElement } from 'react';

interface IInputProps {
  onFocus?: () => void;
  onBlur?: () => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  onChange?: (value: string) => void;
  value: string;
}

const Input = (props: IInputProps): ReactElement => {
  const handleFocus = () => {
    const { onFocus } = props;
    if (onFocus) onFocus();
  };

  const handleBlur = () => {
    const { onBlur } = props;
    if (onBlur) onBlur();
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const { onKeyDown } = props;
    if (onKeyDown) onKeyDown(e);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { onChange } = props;
    const { value } = e.target;
    if (onChange) onChange(value);
  };

  return (
    <input
      type="text"
      maxLength={140}
      value={props.value}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyDown={handleKeyDown}
      onChange={handleChange}
    />
  );
};

export { Input };

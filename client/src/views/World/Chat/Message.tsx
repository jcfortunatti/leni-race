import React, { ReactElement } from 'react';
import { IChatMessage } from '../../../store/types';

const Message = (props: IChatMessage): ReactElement => {
  const { name, message } = props;
  return (
    <div className="chat-message">
      <span>{name}: </span> {message}
    </div>
  );
};

export { Message };

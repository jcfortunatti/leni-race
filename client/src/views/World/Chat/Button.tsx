import React, { ReactElement } from 'react';

interface IButtonProps {
  onClick?: () => void;
}

const Button = (props: IButtonProps): ReactElement => {
  const handleClick = () => {
    const { onClick } = props;
    if (onClick) onClick();
  };

  return <button onClick={handleClick}>Send</button>;
};

export { Button };

import React, { useContext } from 'react';
import { ApplicationContext } from '../../../store';
import "./styles.scss"

const Join = () => {
  const context = useContext(ApplicationContext);

  if (context.name && context.connected) return null;

  const handleNameChange = (name: string) => {
    context.changeName(name)
  };

  const handleJoin = () => {
    if (context.name) {
      context.connectSocket()
    }
  };

  return (
    <div className="join-form-container">
      <div className="join-form">
        <div>
          <Field label="Nickname" value={context.name} onChange={handleNameChange} />
        </div>
        <div className="center">
          <Button label="Join" onClick={handleJoin} />
        </div>
      </div>
    </div>
  );
};

const Field = ({ label, value, onChange }) => {
  const handleChange = (e) => {
    onChange(e.target.value)
  };

  return <div className="input">
    <label>{label}</label>
    <input type="text" value={value} onChange={handleChange} />
  </div>
}

const Button = ({ label, onClick }) => {
  const handleClick = () => {
    onClick()
  };

  return <button className="button" onClick={handleClick}>
    {label}
  </button>
}

export default Join;

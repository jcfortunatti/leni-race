import React, { useContext } from 'react';
import Player from './Objects/Player';
import FieldComponent from './Objects/Field';
import RocketComponent from './Objects/Rocket';
import BoosterComponent from './Objects/Booster';
import { ApplicationContext } from '../../store';
import Stars from './Stars';
import { stars } from '../../settings';

const Draw = () => {
  const { players, self, effects } = useContext(ApplicationContext);
  const playerList = Object.keys(players).filter(id => self ? id !== self.id : true).map(id => players[id])

  return (
    <>
      <FieldComponent />
      <Stars stars={stars} />
      <g>
        {effects.map(effect => {
          switch (effect.type) {
            case 'rocket':
              return <RocketComponent effect={effect} />
            case 'booster':
              return <BoosterComponent booster={effect} />
            default:
              return null;
          }
        })}
        {playerList.map((player) => (<Player key={player.id} player={player} />))}
        {self && <Player isSelf player={self} />}
      </g>
    </>
  );
};

export default Draw;

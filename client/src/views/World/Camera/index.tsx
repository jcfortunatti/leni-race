import React, { useState, useContext, useEffect } from 'react';

import { DefaultComponentProps } from '../../../store/types';
import { ApplicationContext } from '../../../store';
import { mapSize } from '../../../settings';

export interface CameraChildProps extends DefaultComponentProps {
  width?: number;
  height?: number;
  speed?: number
}

const Camera = (props: CameraChildProps) => {
  const context = useContext(ApplicationContext)
  const [position, setPosition] = useState({ x: 0, y: 0 });

  const updatePosition = () => {
    const { self } = context
    if (self) {
      const x = -self.position.x + props.width / 2
      const y = -self.position.y + props.height / 2

      const edgeX = self.position.x < mapSize.width - self.position.x ? 0 : props.width - mapSize.width
      const edgeY = self.position.y < mapSize.height - self.position.y ? 0 : props.height - mapSize.height

      const newX = (x <= 0 && x >= props.width - mapSize.width ? x : edgeX) / (props.speed ? props.speed : 1)
      const newY = (y <= 0 && y >= props.height - mapSize.height ? y : edgeY) / (props.speed ? props.speed : 1)

      if (newX !== position.x || newY !== position.y)
        setPosition({ ...position, x: newX, y: newY })
    }
  }

  useEffect(updatePosition, [context.self?.position])


  return (
    <g transform={`translate(${position.x}, ${position.y})`}>
      {props.children}
    </g>
  );
};

export default Camera;

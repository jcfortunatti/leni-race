import React, { useEffect, useRef, useState } from 'react';
import { DefaultComponentProps } from '../../../store/types';
import ResizeObserver from 'resize-observer-polyfill';
import debounce from 'lodash/debounce';

export interface GameContainerChildrenProps {
  width: number;
  height: number;
}

const GameContainer = (props: DefaultComponentProps) => {
  const container = useRef();
  const [size, setSize] = useState({ width: 0, height: 0 });

  useEffect(() => {
    const currentContainer = container.current;
    const updateComputedStyles = () => {
      if (container && container.current) {
        const { width: strWidth, height: strHeight } = window.getComputedStyle(
          container.current,
        );
        const width = parseInt(strWidth.replace('px', ''));
        const height = parseInt(strHeight.replace('px', ''));
        if (size.width !== width || size.height !== height) {
          setSize({ width, height });
        }
      }
    };
    const observer = new ResizeObserver(debounce(updateComputedStyles, 100));
    observer.observe(currentContainer);
    return () => {
      observer.unobserve(currentContainer);
    };
  });

  return (
    <div className="world" ref={container}>
      <svg width={size.width} height={size.height}>
        {React.Children.map<any, any>(props.children, (child) =>
          React.cloneElement<any, any>(child, {
            ...child.props,
            ...size,
          }),
        )}
      </svg>
    </div>
  );
};
export default GameContainer;

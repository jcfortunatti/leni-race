import React from 'react';

const RocketComponent = (props: { effect }) => {
  const { effect } = props;

  const rotate = (() => {
    if (effect.direction.x === 0)
      return (effect.direction.y > 0 ? 90 : -90) + 90
    return (Math.sign(effect.direction.x) === -1 ? -180 : 0) + Math.atan(effect.direction.y / effect.direction.x) * 180 / Math.PI + 90
  })()

  return (
    <g
      className="rocket"
      transform={`translate(${effect.position.x}, ${effect.position.y}) rotate(${rotate})`}
    >
      <g transform="scale(0.25, 0.25)">
        <g transform={`translate(-280, -90)`}>
          <path
            fill="#fa3757"
            stroke="#13192d"
            strokeWidth="2"
            strokeMiterlimit="10"
            d="M281.6 12.1s79.8 145.2 13.9 291.4l-6.9 18.5h-13.9l-6.9-18.5c-66-146.2 13.8-291.4 13.8-291.4z"
          >
          </path>
          <path
            fill="white"
            stroke="#121625"
            strokeWidth={3}
            strokeMiterlimit={10}
            d="M295.4 303.5l-6.9 18.5h-13.8l-6.9-18.5c8.8 2.1 18 1.9 27.6 0zM306 75.1c-11.2-38.7-24.5-63-24.5-63s-13.2 24.1-24.4 62.5c14.2 4.7 30.9 3.9 48.9.5zM246.9 237.2s36.3 4.9 69.3 0l-2.7 12.7s-32 3.9-64 0l-2.6-12.7zM251 255.5s32.8 4.2 61.1 0l-3.1 10.9s-27.4 3.3-54.9 0l-3.1-10.9z" />
          <path
            fill="white"
            stroke="#121625"
            strokeWidth={3}
            strokeMiterlimit={10}
            d="M283.6 311.9c0 43.6-.9 79-2 79s-2-35.4-2-79 .9-90.6 2-90.6 2 47 2 90.6zM225.7 214.8s-16.6 69.4-1.6 162.7c0 0 7.5-91.1 32.9-101.2 0 0-5.1-17.4-7.2-25.3 0-.1-26-5.9-24.1-36.2zM337.5 214.8s16.6 69.4 1.6 162.7c0 0-7.5-91.1-32.9-101.2 0 0 5.1-17.4 7.2-25.3-.1-.1 26-5.9 24.1-36.2z" />
        </g>
      </g >
    </g >
  );
};

export default RocketComponent;

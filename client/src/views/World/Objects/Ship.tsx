import React, { useContext } from 'react';
import { mapSize } from '../../../settings';
import { ApplicationContext } from '../../../store';
import { IVector } from '../../../store/types';

const shipSize = 40;
const shipDomeSize = Math.round(shipSize * 0.75);
const strokeWidth = Math.round(shipSize * 0.05);

const thrustersWidth = 10;
const thrustersHeight = 6;
const flameWidth = 8;
const flameHeight = 20;

const Ship = ({ direction, boosted, rockets }: { direction: IVector, boosted: boolean, rockets: number }) => {
  const power = (() => {
    return {
      left: direction.x > 0 ? 0.5 * (boosted ? 1.3 : 1) : 0,
      middle: direction.y < 0 ? 1 * (boosted ? 1.3 : 1) : 0,
      right: direction.x < 0 ? 0.5 * (boosted ? 1.3 : 1) : 0,
    }
  })()

  return (
    <g>
      <Thruster x={0} y={shipSize} power={power.middle} />
      <Thruster x={thrustersWidth * 2} y={shipSize - 5} power={power.right} />
      <Thruster x={thrustersWidth * -2} y={shipSize - 5} power={power.left} />


      <g className="player-booster" opacity={boosted ? 1 : 0}>
        <g transform={`translate(${boosted ? -50 : -20}, -35)`}>
          <path
            fill="#fa3757"
            stroke="#13192d"
            strokeWidth="2"
            strokeMiterlimit="10"
            d="m 10 0 a 10 10 0 0 1 10 10 l 0 70 l -20 0 l 0 -70 a 10 10 0 0 1 10 -10 z"
          />
          {power.middle && <path
            d={`M 5 85 a 25 25 0 0 0 ${10 / 2} ${35} a 25 25 0 0 0 ${10 / 2} -${35} z`}
            fill="#ffcd38"
          />}
          <path
            fill="gray"
            stroke="gray"
            strokeWidth="2"
            strokeMiterlimit="10"
            d="M 20 80 a 23 23 0 0 1 3 10 l -26 0 a 23 23 0 0 1 3 -10"
          />
        </g>
        <g transform={`translate(${boosted ? 30 : 0}, -35)`}>
          <path
            fill="#fa3757"
            stroke="#13192d"
            strokeWidth="2"
            strokeMiterlimit="10"
            d="m 10 0 a 10 10 0 0 1 10 10 l 0 70 l -20 0 l 0 -70 a 10 10 0 0 1 10 -10 z"
          />
          {power.middle && <path
            d={`M 5 85 a 25 25 0 0 0 ${10 / 2} ${35} a 25 25 0 0 0 ${10 / 2} -${35} z`}
            fill="#ffcd38"
          />}
          <path
            fill="gray"
            stroke="gray"
            strokeWidth="2"
            strokeMiterlimit="10"
            d="M 20 80 a 23 23 0 0 1 3 10 l -26 0 a 23 23 0 0 1 3 -10"
          />
        </g>
      </g>

      <circle r={shipSize + strokeWidth} fill="black" />
      <circle r={shipSize} fill="#EF3D5A" />
      <circle r={shipDomeSize + strokeWidth} fill="black" />

      <circle r={shipDomeSize} fill="#1f2740" />
      <image x="-40" y="-40" width="80" height="80" xlinkHref="/logo/leni.svg" />

      <path fill="#EF3D5A"
        d="M0 0m-40,0a40,40,0 1,0 80,0a 40,40 0 1,0 -80,0zM0 0m-30,0a30,30,0 0,1 60,0a 30,30 0 0,1 -60,0z" />

      <path stroke="black"
        d="M0 0m-30,0a30,30,0 1,0 60,0a 30,30 0 1,0 -60,0zM0 0m-30,0a30,30,0 0,1 60,0a 30,30 0 0,1 -60,0z" />

      <circle r={shipDomeSize} fill="#69B3B250" />

      {new Array(rockets).fill(0).map((_, i) => (
        <circle
          cx={((shipSize - shipDomeSize) / 2 + shipDomeSize) * Math.cos(i * Math.PI / 5 - 9 * Math.PI / 10)}
          cy={((shipSize - shipDomeSize) / 2 + shipDomeSize) * Math.sin(i * Math.PI / 5 - 9 * Math.PI / 10)}
          r={3}
          fill="#FFCB31"
          stroke="black"
        />))}
    </g >
  );
};

const Thruster = ({ x, y, power }) => {
  const actualFlameHeight = flameHeight * power;
  return <g
    transform={`translate(${x - thrustersWidth / 2}, ${y})`}
  >
    <path
      d={`M ${(thrustersWidth - flameWidth) / 2} ${thrustersHeight} a 15 15 0 0 0 ${flameWidth / 2} ${actualFlameHeight} a 15 15 0 0 0 ${flameWidth / 2} -${actualFlameHeight} z`}
      fill="#ffcd38"
    />
    <rect
      width={thrustersWidth + strokeWidth}
      height={thrustersHeight + strokeWidth}
      fill="black"
    />
    <rect
      width={thrustersWidth}
      height={thrustersHeight}
      fill="#FFCB31"
    />
  </g>
}

export default Ship;

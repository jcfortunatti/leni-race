import React from 'react';
import { mapSize, pallete } from '../../../settings';

const Field = () => {
  return (
    <>
      <rect {...mapSize} fill={pallete.background} />
    </>
  );
};

export default Field;

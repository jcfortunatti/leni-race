import React from 'react';

const BoosterComponent = (props: { booster }) => {
	const { booster } = props;

	return (
		<g
			className="booster"
			transform={`translate(${booster.position.x}, ${booster.position.y})`}
		>
			<g transform={`translate(-30, -30)`}>
				<rect width={60} height={60} stroke="black" fill="#ffca2f" />
				<rect x={5} y={5} width={50} height={50} stroke="black" fill="black" />
				<g transform={`scale(0.36)`}>
					<g transform={`translate(115, 32) rotate(45)`}>
						<path
							d={`M -20 85 a 25 25 0 0 0 ${10 / 2} ${35} a 25 25 0 0 0 ${10 / 2} -${35} z`}
							fill="#ffcd38"
						/>
						<path
							d={`M 30 85 a 25 25 0 0 0 ${10 / 2} ${35} a 25 25 0 0 0 ${10 / 2} -${35} z`}
							fill="#ffcd38"
						/>
						<path
							transform={`translate(-25, 0)`}
							fill="#fa3757"
							stroke="#13192d"
							strokeWidth="2"
							strokeMiterlimit="10"
							d="m 10 0 a 10 10 0 0 1 10 10 l 0 70 l -20 0 l 0 -70 a 10 10 0 0 1 10 -10 z"
						/>
						<path
							transform={`translate(-25, 0)`}
							fill="gray"
							stroke="gray"
							strokeWidth="2"
							strokeMiterlimit="10"
							d="M 20 80 a 23 23 0 0 1 3 10 l -26 0 a 23 23 0 0 1 3 -10"
						/>
						<path
							transform={`translate(25, 0)`}
							fill="#fa3757"
							stroke="#13192d"
							strokeWidth="2"
							strokeMiterlimit="10"
							d="m 10 0 a 10 10 0 0 1 10 10 l 0 70 l -20 0 l 0 -70 a 10 10 0 0 1 10 -10 z"
						/>
						<path
							transform={`translate(25, 0)`}
							fill="gray"
							stroke="gray"
							strokeWidth="2"
							strokeMiterlimit="10"
							d="M 20 80 a 23 23 0 0 1 3 10 l -26 0 a 23 23 0 0 1 3 -10"
						/>
						<rect x={-5} y={20} width={30} height={5} fill="gray" />
						<rect x={-5} y={35} width={30} height={5} fill="gray" />
					</g>
				</g>
			</g>
		</g >
	);
};

export default BoosterComponent;

import React from 'react';
import { IPlayer } from '../../../store/types';
import Ship from './Ship';

const PlayerComponent = (props: { player: IPlayer; isSelf?: boolean }) => {
  const { player, isSelf } = props;

  return (
    <g
      className={'player ' + (isSelf ? ' self' : '') + (player.exploded ? ' exploded' : '')}
      key={player.id}
      transform={`translate(${player.position.x}, ${player.position.y})`}
    >
      <defs>
        <filter x="-5%" y="0%" width="110%" height="120%" id={`${player.id}_name_background`}>
          <feFlood flood-color="black" />
          <feComposite in="SourceGraphic" operator="and" />
        </filter>
      </defs>
      <g
        transform={`translate(0, -50)`}
      >
        <text fontSize={15} filter={`url(#${player.id}_name_background)`} textAnchor="middle" fill='white' >{player.name} </text>
        <rect fill="black" x={-40} y={-27} width={80} height={12} stroke="black" />
        <rect fill="green" x={-40} y={-27} width={player.life * 80 / 100} height={10} />
        <rect fill="#FFCB31" x={-40} y={-15} width={player.rocketCooldown * 80 / 100} height={2} />
        <line stroke="black" x1={-20} y1={-27} y2={-15} x2={-20} />
        <line stroke="black" x1={0} y1={-27} y2={-15} x2={0} />
        <line stroke="black" x1={20} y1={-27} y2={-15} x2={20} />
      </g>
      <Ship direction={player.direction} boosted={player.boosted} rockets={player.rockets} />
      {player.exploded ? <image className="explosion-svg" x="-40" y="-40" width="80" height="80" xlinkHref="/logo/explosion.svg" /> : null}
    </g >
  );
};

export default PlayerComponent;

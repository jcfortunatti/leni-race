import React from 'react';
const Star = ({ x, y, fill, radius }) => {
  return (
    <>
      <circle r={radius} cx={x} cy={y} fill={fill} />
    </>
  );
};

export default Star;

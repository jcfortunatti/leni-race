import React, { useMemo, memo } from 'react';
import Star from './Objects/Star';

const Stars = ({ stars }) => {
  return (
    <g>
      {stars.map((s, i) => <Star key={i} {...s} />)}
    </g>
  );
};

export default memo(Stars, (prev, next) => {
  return true;
});

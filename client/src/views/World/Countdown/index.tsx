import React, { useContext } from 'react';
import { ApplicationContext } from '../../../store';
import "./styles.scss"

const Countdown = () => {
  const context = useContext(ApplicationContext);

  if (!context.name || !context.connected || context.gameEnded || context.countdown === 0) return null;

  return (
    <div className="countdown-container">
      <div className="countdown">
        <div>{context.countdown}</div>
      </div>
    </div>
  );
};

export default Countdown;

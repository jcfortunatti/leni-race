import React, { useContext } from 'react';
import { ApplicationContext } from '../../../store';
import "./styles.scss"

const ScoreBoard = () => {
  const context = useContext(ApplicationContext);

  if (!context.name || !context.connected || context.gameEnded) return null;

  return (
    <div className="scoreboard-container">
      <div className="scoreboard">
        <div className="title">
          <h2>Positions</h2>
        </div>
        <div className="box-list">
          {context.placements.map((player, i) =>
            <div className="box">
              {i === 0 && <RecognitionFirst />}
              {i === 1 && <RecognitionSecond />}
              {i === 2 && <RecognitionThird />}
              <div>{player.name}</div>
            </div>
          )}
          {Object.values(context.players)
            .filter(player => !context.placements.find(p => p.id === player.id))
            .sort((a, b) => a.position.y - b.position.y).map((player, i) =>
              <div className="box">
                {(context.placements.length + i) === 0 && <RecognitionFirst />}
                {(context.placements.length + i) === 1 && <RecognitionSecond />}
                {(context.placements.length + i) === 2 && <RecognitionThird />}
                <div>{player.name}</div>
              </div>
            )}
        </div>
      </div>
    </div>
  );
};

const Placements = () => {
  const context = useContext(ApplicationContext);
  if (!context.gameEnded) return null;

  return (
    <div className="scoreboard-container finished">
      <div className="scoreboard">
        <div className="title">
          <h2>Placements - {context.countdown ? `(New game in: ${context.countdown})` : null}</h2>
        </div>
        <div className="box-list">
          {context.placements.map((player, i) =>
            <div className="box">
              {i === 0 && <RecognitionFirst />}
              {i === 1 && <RecognitionSecond />}
              {i === 2 && <RecognitionThird />}
              <div>{player.name}</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

const Recognition = ({ children }) => {
  return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 94.8 106.9" className="icon-recongnition">
    <path className="st0" d="M0 90.4l22-3.7 7.1 20.2L55.6 60 26.4 43.6z" />
    <path className="st1" d="M22 86.7L0 90.4l26.4-46.8 15.1 8.5z" />
    <path className="st0" d="M66.8 106.9l6.8-21.2 21.2 2.9-29.3-45-28 18.2z" />
    <path className="st1" d="M73.6 85.7l-6.8 21.2-29.3-45.1L52 52.4z" />
    <g>
      <circle cx="47.7" cy="40.9" r="40.9" fill="#12192c" />
      <circle fill="#69b2b1" cx="47.7" cy="40.9" r="34.7" />
    </g>
    {children}
  </svg>
}

const RecognitionFirst = () => {
  return <Recognition>
    <g transform="translate(4, -5)">
      <g transform="translate(-15, 10) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
      <g transform="translate(15, 10) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
      <g transform="translate(0, 0) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
    </g>
  </Recognition>
}

const RecognitionSecond = () => {
  return <Recognition>
    <g transform="translate(4, -7)">
      <g transform="translate(-15, 10) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
      <g transform="translate(15, 10) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
    </g>
  </Recognition>
}

const RecognitionThird = () => {
  return <Recognition>
    <g transform="translate(4, 3)">
      <g transform="translate(0, 0) scale(0.9)">
        <path
          fill="#ffcb2f" stroke="#12192c"
          stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" stroke-miterlimit="10"
          d="M47.7 18.1l7.1 14.4 15.9 2.4-11.5 11.2 2.7 15.8-14.2-7.5-14.2 7.5 2.7-15.8-11.5-11.2 15.9-2.4z" />
      </g>
    </g>
  </Recognition>
}

export { ScoreBoard, Placements };

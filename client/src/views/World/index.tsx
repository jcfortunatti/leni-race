import React, { ReactElement } from 'react';
import Draw from './Draw';
import { Container, Wrapper } from '../../layout';
import { stars2 } from '../../settings';
import GameChat from './Chat';
import GameContainer from './GameContainer';
import Join from './Join';
import { ScoreBoard, Placements } from './ScoreBoard';
import Camera from './Camera';
import Countdown from './Countdown';
import "./styles.scss"
import Stars from './Stars';

const World = (): ReactElement => {
  return (
    <Wrapper>
      <Container>
        <GameContainer>
          <Camera>
            <Draw />
          </Camera>
          <Camera speed={2}>
            <Stars stars={stars2} />
          </Camera>
          <GameChat />
        </GameContainer>
        <Join />
        <ScoreBoard />
        <Placements />
        <Countdown />
      </Container>
    </Wrapper>
  );
};

export default World;

import io from 'socket.io-client';
import { playSound } from './sounds';
import {
  ISocketEventSubscribers,
  ILoginSuccessPayload,
  IPlayer,
  IChatMessage,
  IUpdatePayload,
  IVector,
} from './types';
import {
  LOGIN_SUCCESS,
  PLAYER_JOIN,
  PLAYER_LEAVE,
  PLAYER_MESSAGE,
  UPDATE,
  REQUEST_SEND_MESSAGE,
  REQUEST_KEY_PRESS,
  REQUEST_DIRECTION_CHANGE,
} from './constants';

class RoomSocket {
  _socket: SocketIOClient.Socket;
  _on: ISocketEventSubscribers = {};

  constructor(roomId: string, name: string) {
    const { protocol, host } = window.location;
    const socketURL = `${protocol}//${host.replace(
      ':8000',
      '',
    )}:8081${roomId}?name=${name}`;

    this._socket = io(socketURL, {
      path: `/ws`,
      autoConnect: false,
    });

    this._socket.on(LOGIN_SUCCESS, (payload: ILoginSuccessPayload) => {
      if (this._on[LOGIN_SUCCESS]) this._on[LOGIN_SUCCESS](payload);
    });

    this._socket.on(PLAYER_JOIN, (payload: IPlayer) => {
      playSound(PLAYER_JOIN);
      if (this._on[PLAYER_JOIN]) this._on[PLAYER_JOIN](payload);
    });

    this._socket.on(PLAYER_LEAVE, (payload: IPlayer) => {
      playSound(PLAYER_LEAVE);
      if (this._on[PLAYER_LEAVE]) this._on[PLAYER_LEAVE](payload);
    });

    this._socket.on(PLAYER_MESSAGE, (payload: IChatMessage) => {
      playSound(PLAYER_MESSAGE);
      if (this._on[PLAYER_MESSAGE]) this._on[PLAYER_MESSAGE](payload);
    });

    this._socket.on(UPDATE, (player: IUpdatePayload) => {
      if (this._on[UPDATE]) this._on[UPDATE](player);
    });
  }

  connect = (): void => {
    this._socket.connect();
  };

  disconnect = (): void => {
    this._socket.disconnect();
    delete this._socket;
  };

  _sendMessage = (
    type: string,
    payload: { [key: string]: string | IVector },
  ): void => {
    this._socket.emit(type, payload);
  };

  requestDirectionChange = (direction: IVector): void =>
    this._sendMessage(REQUEST_DIRECTION_CHANGE, { direction });
  requestKeyPress = (keyCode: string): void =>
    this._sendMessage(REQUEST_KEY_PRESS, { keyCode });
  requestSendMessage = (message: string): void =>
    this._sendMessage(REQUEST_SEND_MESSAGE, { message });

  subscribeLoginSuccess = (
    callback: (payload: ILoginSuccessPayload) => void,
  ): void => {
    this._on[LOGIN_SUCCESS] = (payload) => callback(payload);
  };
  subscribePlayerJoin = (callback: (payload: IPlayer) => void): void => {
    this._on[PLAYER_JOIN] = (payload) => callback(payload);
  };
  subscribePlayerLeave = (callback: (payload: IPlayer) => void): void => {
    this._on[PLAYER_LEAVE] = (payload) => callback(payload);
  };
  subscribePlayerMessage = (callback: (payload: IChatMessage) => void): void => {
    this._on[PLAYER_MESSAGE] = (payload) => callback(payload);
  };
  subscribeUpdate = (callback: (payload: IUpdatePayload) => void): void => {
    this._on[UPDATE] = (payload) => callback(payload);
  };
}
export default RoomSocket;

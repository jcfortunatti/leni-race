import React from 'react';
import { IApplicationContext } from './types';
import { REQUEST_SEND_MESSAGE } from './constants';

export const ApplicationContext = React.createContext<IApplicationContext>({
  connected: false,
  name: '',
  self: null,
  countdown: null,
  players: {},
  effects: [],
  placements: [],
  gameEnded: false,
  messages: [],
  directionKeysPressed: [],
  disabledKeyboard: false,
  changeName: () => { },
  connectSocket: () => { },
  disconnectSocket: () => { },
  disableKeyboard: () => { },
  enableKeyboard: () => { },
  [REQUEST_SEND_MESSAGE]: () => { },
});

export const ApplicationContextConsumer = ApplicationContext.Consumer;
export { ApplicationContextProvider } from './provider';

import { ISocketEvents } from './types';
import { PLAYER_LEAVE, PLAYER_MESSAGE, PLAYER_JOIN } from './constants';

export const playSound = (soundCode: ISocketEvents): void => {
  switch (soundCode) {
    case PLAYER_LEAVE:
      new Audio(`sounds/${PLAYER_LEAVE}.ogg`).play();
      break;
    case PLAYER_JOIN:
      new Audio(`sounds/${PLAYER_JOIN}.ogg`).play();
      break;
    case PLAYER_MESSAGE:
      new Audio(`sounds/${PLAYER_MESSAGE}.ogg`).play();
  }
};

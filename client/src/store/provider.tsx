import React, { ReactNode } from 'react';
import RoomSocket from './socket';
import { ApplicationContext } from '.';
import {
  IVector,
  ApplicationContextProviderState,
  DefaultComponentProps,
} from './types';
import {
  REQUEST_SEND_MESSAGE,
  ALLOWED_ACTION_KEYS,
  ALLOWED_DIRECTION_KEYS,
} from './constants';

export class ApplicationContextProvider extends React.Component<
  DefaultComponentProps,
  ApplicationContextProviderState
  > {
  _socket: RoomSocket = null;
  roomID: string = '/always-open';

  constructor(props: DefaultComponentProps) {
    super(props);
    this.state = {
      connected: false,
      name: "",
      self: null,
      countdown: null,
      messages: [],
      effects: [],
      players: {},
      placements: [],
      gameEnded: false,
      directionKeysPressed: [],
      disabledKeyboard: false,
    };
  }

  componentDidMount(): void {
    document.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('keyup', this.handleKeyUp);

  }

  componentWillUnmount(): void {
    document.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('keyup', this.handleKeyUp);
  }

  changeName = (name: string): void => {
    localStorage.setItem('game', JSON.stringify({ name }));
    this.setState({ ...this.state, name });
  };

  connectSocket = (): void => {
    this._socket = new RoomSocket(this.roomID, this.state.name);

    this._socket.subscribeLoginSuccess((payload) => {
      this.setState({
        ...this.state,
        ...payload,
        connected: true,
      });
    });
    this._socket.subscribePlayerJoin((player) => {
      this.setState({
        ...this.state,
        players: {
          ...this.state.players,
          [player.id]: player,
        },
        messages: [
          ...this.state.messages,
          {
            name: 'GameServer',
            message: `Player ${player.name} has joined.`,
          },
        ],
      });
    });
    this._socket.subscribePlayerLeave((player) => {
      if (this.state.players[player.id]) {
        this.setState({
          ...this.state,
          players: Object.keys(this.state.players).reduce((result, id) => {
            if (id !== player.id && this.state.players[id])
              result[id] = this.state.players[id];
            return result;
          }, {}),
          messages: [
            ...this.state.messages,
            {
              name: 'GameServer',
              message: `Player ${this.state.players[player.id].name} left.`,
            },
          ],
        });
      }
    });

    this._socket.subscribeUpdate((payload) => {
      this.setState({
        ...this.state,
        ...payload,
        self: this.state.self
          ? { ...payload.players[this.state.self.id] }
          : this.state.self,
      });
    });

    this._socket.subscribePlayerMessage((payload) => {
      this.setState({
        ...this.state,
        messages: [...this.state.messages, payload],
      });
    });

    this._socket.connect();
  };

  disconnectSocket = (): void => {
    if (this._socket) {
      this.setState({ ...this.state, connected: false });
      this._socket.disconnect();
      this._socket = null;
    }
  };

  requestSendMessage = (message: string): void => {
    if (this._socket) {
      this._socket.requestSendMessage(message);
    }
  };

  handleActionKeyPress = (keyCode: string): void => {
    if (this._socket) {
      this._socket.requestKeyPress(keyCode);
    }
  };

  handleDirectionChanged = (): void => {
    if (this._socket) {
      const direction: IVector = { x: 0, y: 0 };
      if (this.state.directionKeysPressed.includes('ArrowLeft')) direction.x = -1;
      if (this.state.directionKeysPressed.includes('ArrowRight')) direction.x = 1;
      if (this.state.directionKeysPressed.includes('ArrowUp')) direction.y = -1;
      if (this.state.directionKeysPressed.includes('ArrowDown')) direction.y = 1;
      this._socket.requestDirectionChange(direction);
    }
  };

  //keyboard

  disableKeyboard = (): void => {
    this.setState({ disabledKeyboard: true });
  };

  enableKeyboard = (): void => {
    this.setState({ disabledKeyboard: false });
  };
  handleKeyUp = (e): void => {
    const { code } = e;
    const { disabledKeyboard } = this.state;
    if (!disabledKeyboard)
      if (ALLOWED_DIRECTION_KEYS.includes(code)) {
        this.setState(
          {
            directionKeysPressed: this.state.directionKeysPressed.filter(
              (k) => k !== code,
            ),
          },
          this.handleDirectionChanged,
        );
      }
  };

  handleKeyDown = (e): void => {
    const { disabledKeyboard } = this.state;
    const { code } = e;
    if (!disabledKeyboard) {
      if (ALLOWED_ACTION_KEYS.includes(code)) {
        this.handleActionKeyPress(code);
      } else if (ALLOWED_DIRECTION_KEYS.includes(code) && !this.state.directionKeysPressed.includes(code)) {
        this.setState(
          {
            directionKeysPressed: [...this.state.directionKeysPressed, code],
          },
          this.handleDirectionChanged,
        );
      }
    }
  };

  render(): ReactNode {
    return (
      <ApplicationContext.Provider
        value={{
          ...this.state,
          changeName: this.changeName,
          connectSocket: this.connectSocket,
          disconnectSocket: this.disconnectSocket,
          disableKeyboard: this.disableKeyboard,
          enableKeyboard: this.enableKeyboard,
          [REQUEST_SEND_MESSAGE]: this.requestSendMessage,
        }}
      >
        {this.props.children}
      </ApplicationContext.Provider>
    );
  }
}

export const REQUEST_DIRECTION_CHANGE = 'request_direction_change';
export const REQUEST_KEY_PRESS = 'request_key_press';
export const REQUEST_SEND_MESSAGE = 'request_send_message';

export const UPDATE = 'update';
export const LOGIN_SUCCESS = 'login_success';
export const PLAYER_JOIN = 'player_join';
export const PLAYER_LEAVE = 'player_leave';
export const PLAYER_MESSAGE = 'player_message';

export const ALLOWED_DIRECTION_KEYS = [
  'ArrowLeft',
  'ArrowUp',
  'ArrowRight',
];
export const ALLOWED_ACTION_KEYS = ['Space', 'KeyQ', 'KeyW'];

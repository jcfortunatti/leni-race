import {
  LOGIN_SUCCESS,
  PLAYER_JOIN,
  PLAYER_LEAVE,
  PLAYER_MESSAGE,
  UPDATE,
  REQUEST_SEND_MESSAGE,
} from './constants';
import { ReactElement } from 'react';

export interface IVector {
  x: number;
  y: number;
}

export interface IPlayer {
  id: string;
  name: string;
  direction: IVector;
  position: IVector;
  angle: number;
  exploded: boolean;
  life: number;
  boosted: boolean;
  rockets: number;
  rocketCooldown: number;
}

export interface IEffect {
  type: string;
  direction: IVector;
  position: IVector;
  angle: number;
}

export interface IChatMessage {
  name: string;
  message: string;
}

export interface IUpdatePayload {
  players: { [key: string]: IPlayer };
  placements: IPlayer[];
  seconds: number;
  gameEnded: boolean;
}

export interface ILoginSuccessPayload extends IUpdatePayload {
  self: IPlayer;
}

export interface ApplicationContextProviderState {
  connected: boolean,
  name: string;
  self?: IPlayer;
  countdown?: number;
  messages: IChatMessage[];
  players: { [key: string]: IPlayer };
  effects: IEffect[],
  placements: IPlayer[];
  gameEnded: boolean;
  directionKeysPressed: string[];
  disabledKeyboard: boolean;
}

export interface IApplicationContext extends ApplicationContextProviderState {
  changeName: (name: string) => void;

  connectSocket: () => void;
  disconnectSocket: () => void;

  disableKeyboard: () => void;
  enableKeyboard: () => void;

  [REQUEST_SEND_MESSAGE]: (message: string) => void;
}

export type ISocketEvents =
  | typeof UPDATE
  | typeof LOGIN_SUCCESS
  | typeof PLAYER_JOIN
  | typeof PLAYER_LEAVE
  | typeof PLAYER_MESSAGE;

export type ISocketEventSubscribers = {
  [UPDATE]?: (payload: IUpdatePayload) => void;
  [LOGIN_SUCCESS]?: (payload: ILoginSuccessPayload) => void;
  [PLAYER_JOIN]?: (payload: IPlayer) => void;
  [PLAYER_LEAVE]?: (payload: IPlayer) => void;
  [PLAYER_MESSAGE]?: (payload: IChatMessage) => void;
};

export type DefaultComponentProps = Readonly<{
  children?: ReactElement | ReactElement[];
}>;

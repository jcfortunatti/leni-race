export const routes = [{ name: 'Builder', route: '/' }];

export const mapSize = {
  width: 2000,
  height: 50000,
};

export const pallete = {
  background: '#121625',
  nebula: '#212539',
  stars: [
    '#f8e299',
    '#f8f3dc',
    '#595a5e'
  ],
};


export const stars = require('./stars.json');
export const stars2 = require('./stars2.json');

// const generateStars = () => {
//   const generateStar = () => {
//     const fill = pallete.stars[Math.trunc(Math.random() * pallete.stars.length)]
//     const x = Math.round(Math.random() * mapSize.width);
//     const y = Math.round(Math.random() * mapSize.height);
//     const radius = Math.random() * 5;
//     return { x, y, fill, radius }
//   }

//   return new Array(10000).fill(0).map(_x => generateStar())
// }